---
layout: post
title:  "Static Site Generator and Native Translations: Why so hard?"
date:   2019-10-08 11:35:35 +0200
author: "Carl Schwan"
lang: en
---

## Context

At the Akademy (the annual world summit of KDE), I wanted to get rid of the old
[PHP Capacity Framework](https://cgit.kde.org/websites/capacity.git/tree/),
developed by KDE a long time ago to have a shared theme between all KDE websites.

This framework is very old, has a lot of useless functionality (e.g. a text editor
that create php files that then need to uploaded to svn or git) and probably some
security vulnerabilities.

The biggest problem with the migration, disregarding the work needed to move all the
content to a new framework, are the translations. [kde.org](https://kde.org) is
partially translated and the part that are already translated should remains translated.

## Gettext in KDE

KDE use Gettext for the entire translation process. A script is generating pot files from
the git repositories, those files are then placed in a SVN directory and the translators
are then taking care of updating their po files. It's a bit more difficult than needed, but
Gettext provides us, with an entire infrastructure to merge the translations and extract
the strings from the repository. KDE has also a GUI to edit po files: [Lokalize]
(https://kde.org/applications/development/org.kde.lokalize).

## 
So I tried to look into other framework.
