---
layout: page
title: About
---

My name is Carl Schwan and I'm 25 years old.

I grow up in a French and German family and I'm currently living in Berlin,
previously I lived 6 years in Saarbrücken (Germany), 10 years in Nancy (France)
and 8 years in a small village in the Auvergne (also France). So please feel
free to contact me in French, German or English.

I'm currently working at [G10 Code GmbH](https://g10code.com/) and doing
GnuPG/KDE stuff where, but the opinions expressed in this blog are my own and
not necessarily those of my employer.

In my free time, I contribute to the [KDE](https://kde.org) project, there
I maintain many websites, work on many apps for both Plasma Desktop and Plasma
Mobile, help with the promotion and occasionally work on the documentation.

My other hobbies are cycling, cooking and playing board games.

You can contact me at [carlschwan@kde.org](mailto:carlschwan@kde.org) or
[carl@carlschwan.eu](mailto:carl@carlschwan.eu) which both redirect to the
same mailbox anyway.
