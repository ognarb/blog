---
title: 2022 in review
draft: true
---

Following the example of [Kai](https://blog.broulik.de/2022/12/looking-back-at-2022/), I am also taking a step back ad reflect on some of the things I did in the open source world in the last twelve months.

## Kalendar

Kalendar is calendar application written completely in QML and Kirigami which I have been working with Claudio since last year. In 2022, I started working on slowly but steadily making Kalendar a complete groupware solution. I added an [address book](2022/06/14/kalendar-contact-book-current-progress/) in Kalendar and started the work to also make Kalendar a mail client (currently it is read only and hidden by default).

And due to the application getting more and more complex, I started modularizing the code base, creating separate QML plugins for each main part of the application and also introduce more and more unit tests.

## NeoChat

NeoChat is a matrix client also written completely with QML and Kirigami. This year, I unfortunatrly didn't had time to contribute much code to NeoChat but instead focused on doing code review. I'm really happy to see that we got a new contributor James Graham who in the past months made gigantic changes to NeoChat and helped Tobias push NeoChat forward.

I did made some small changes here and there, one of them is that ported multiple settings pages to the new Kirigami MobileForm component (so now you know who to blame in case you don't like them).

![NeoChat new about page](neochat-about.png)

2022 is also the year where libQuotient (the library that NeoChat use to interract with a Matrix server) finally was [released with end-to-end-encryption support](https://github.com/quotient-im/libQuotient/releases/tag/0.7.0) which was contributed by Tobias and myself (but mostly Tobias).

## Tokodon

Tokodon is mastodon client which is also written with QML and Kirigami. I did a lot of work on it in the recent months. I'm quickly implementing most of the mastodon API and making Tokodon feature complete. It's now support custom emojis, search, conversations, multiple accounts, polls, real time updates, editing your account, sensitive attachement, ...

See the [22.11.1 and 22.11.2 announcement](https://carlschwan.eu/2022/12/18/tokodon-22.11.2-release/) and [22.12 announcement](https://carlschwan.eu/2022/12/31/tokodon-22.12.0-release/).

## Travels and Itinerary

2022 is also the year, I started traveling again. I went to the Linux App Summit in Trento (Italy) and ate wonderful pizzas. I also went to the Akademy in Barcelona (Catalunya) and also ate wonderful pizzas.

Aside from KDE events, I went 3 weeks in the south of France and Spain with an Interrail ticket and I visited Bordeaux, Arcachon, 

## Nextcloud

