---
categories: kde wiki
date: "2018-11-18T11:35:35Z"
locale: en
title: Contributing to the kde userbase wiki
---



This is the story about how I started more than one month ago contributing to the KDE project.

So, one month ago, I found a task on the [Phabricator instance](https://phabricator.kde.org/T9142) from KDE, about the deplorable state of the [KDE userbase wiki](https://userbase.kde.org/). The wiki contains a lot of screenshots dating back to the KDE 4 era and some are even from the KDE 3 era. It’s a problem, because a wiki is something important in the user experience and can be really useful for new users and experienced ones alike.

Lucky for us, even though Plasma and the KDE applications did change a lot in the last few years, most of the changes are new features and UI/UX improvements, so most of the information are still up-to-date. So most of the work is only updating screenshots. But up-to-date screenshots are also quite important, because when the user see the old screenshots, he can think that the instructions are also outdated.

So I started, updating the screenshots one after the other. (Honestly when I started, I didn’t think it would take so long, not because the process was slow or difficult, but because of the amount of outdated screenshots.)

But I also learned a lot about KDE doing this. For example did you know that Blink (Chrome webengine) is a fork of Webkit (Safari webengine) and Webkit is a fork of KHTML (Konqueror webengine). I also learned about the existence of lesser-known KDE apps, for example Kile (a latex IDE), Calligra (an office suite), KFloppy (a floppy disk editor), …

## How to update the screenshots

As a non-native english speaker, I found out updating screenshots and quickly checking if the information is up-to-date is easier as I first though. There aren’t a lot of requirements, you only need a Phabricator account and the default Breeze theme installed. The Phabricator account is easy to create and the default theme should already be installed.

Then for each wiki entry, you only need to download the software, find all outdated screenshots in the wiki entry, take a new screenshot for each old screenshot, and upload the new screenshot.

![Upload new file version in userbase kde](/assets/img/upload_new_version.png)

For all icons, I quickly generated a png from the svg file with the following command:

`convert -density 1200 -resize 128x128 -background transparent /usr/share/icons/breeze/apps/48/okular.svg okular.png`

It’s not finished, there are still a lot of outdated screenshots in the wiki, but every day the amount decreases. :)


## Geting involved

And you, dear reader, can also help. Like I said: this job doesn’t need any programming skills or perfect english skills, just a bit of motivation. If you need help, there are some instructions available to get started editing the wiki: [Start Contributing](https://userbase.kde.org/Tasks_and_Tools), [Markup help](https://userbase.kde.org/Toolbox), [Quick Start](https://userbase.kde.org/Quick_Start). You can also contact me: on the fediverse [@carl@linuxrocks.online](https://linuxrocks.online/@carl) or on reddit ([/u/ognarb1](https://reddit.com/u/ognarb1)).

Thanks to XYQuadrat for proofreading this blog post. :D

