---
title: Kirigami Addons 1.4
date: "2024-07-23T21:20:35Z"
categories:
  - Free Software
  - KDE
tags:
  - KDE
  - Kirigami Addons
hideHeaderImage: true
image: shortcut-editor.png
comments:
  host: floss.social
  username: carlschwan
  id: 112837806738133082
---

Kirigami Addons 1.4 is out! This release introduce a new module to manage
actions similar to that we can find in the QtWidgets world with KXmlGui. This
was not written from scratch but upstream the existing infrastructure from
Merkuro (ex-Kalendar) and Marknote. These two applications have already been
ported to this new module and more like Tokodon or
[KDE Keychain](https://carlschwan.eu/2024/07/04/keychain-development-update-yubikey-support/)
will follow soon.

This includes a shortcut editor to assign and modify the shortcuts of an
application and a command bar to quickly search and trigger actions

![Shortcut editor](shortcut-editor.png)

![Command bar](commandbar.png)

Similar to KXmlGui, the actions are defined in C++, which allows to make use
[KStandardActions](https://api.kde.org/frameworks/kconfig/html/namespaceKStandardActions.html)
and get consistent shortcuts accross all your applications.

```cpp
class MyApplication : public AbstractKirigamiApplication
{
    Q_OBJECT
    QML_ELEMENT

public:
    explicit MyApplication(QObject *parent = nullptr);

    void setupActions() override;

Q_SIGNALS:
    void addNotebook();
};
 
MyApplication::MyApplication(QObject *parent)
    : AbstractKirigamiApplication(parent)
{
     setupActions();
}
 
void MyApplication::setupActions()
{
    AbstractKirigamiApplication::setupActions();
 
    auto actionName = QLatin1String("add_notebook");
    if (KAuthorized::authorizeAction(actionName)) {
        auto action = mainCollection()->addAction(actionName, this, &MyApplication::addNotebook);
        action->setText(i18nc("@action:inmenu", "New Notebook"));
        action->setIcon(QIcon::fromTheme(QStringLiteral("list-add-symbolic")));
        mainCollection()->addAction(action->objectName(), action);
        mainCollection()->setDefaultShortcut(action, QKeySequence(Qt::CTRL | Qt::SHIFT | Qt::Key_N));
    }
}
```

These new actions can then be used from QML thanks to the new
`Kirigami.Action::fromQAction` property.

```qml
import org.kde.kirigamiaddons.statefulapp as StatefulApp
import org.kde.kirigamiaddons.settings as Settings
 
StatefulApp.StatefulWindow {
    id: root
 
    windowName: 'Main'
    application: MyApplication {
        configurationView: Settings.ConfigurationView { ... }
    }

    Kirigami.Action {
        fromQAction: MyApplication.action('add_notebook')
    }

    Connections {
        target: MyApplication

        function onAddNotebook(): void {
            ...
        }
    }
}
```

There is a new template available in KAppTemplate, which allows you to
kickstart your new Kirigami application with the basic skeleton with this new
module and other "Kirigami Addons" modules.

## Other Changes

The FormCard design was tweaked a bit more when using a dark theme, thanks to
James and Joshua for their feedback.

Speaking of FormCard, with the development of KeyChain, I ended up adding a new
component to the FormCard collection: FormTextAreaDelegate. This component
is the equivalent of FormTextFieldDelegate but with a TextArea instead.
FormComboBoxDelegate and FormTextFieldDelegate also received a bunch of new
properties and functions to proxy the underlying QtQuick.Controls component.

Evgeniy Harchenko tweaked a bit the headers of the TableView component.

Finally, a new contributor Andreas Gattringer fixed a crash in the video
maximizing component which was affecting NeoChat. 

## Packager Section

You can find the package on
[download.kde.org](https://download.kde.org/stable/kirigami-addons/kirigami-addons-1.4.0.tar.xz.sig.mirrorlist)
and it has been signed with my [GPG key](/gpg-02325448204e452a/).