---
title: Working with QtWidgets
---

If you know me, you probably know that I have been a big than of QML and
QtQuick as technology. I'm started NeoChat, Tokodon, Arianna, Kalendar
(now known as Merkuro), ported KWordQuiz to QML and regularly contribute
to an handful of other QML projects inside KDE.

Recently I started working at GnuPG on Kleopatra and other Qt components
of the [GPG4Win](https://www.gpg4win.org/) and
[GnuPG-VS Desktop](https://gnupg.com/gnupg-desktop.html) product, which are
all QtWidgets based. This has been a quite rewarding learning experience as
it is both very similar and different to QtQuick in various aspects.

These two technologies share a lot in term of data structures with all the
containers being the same (QVector, QString, QHash, ...) as well as the
concept of models (QAbstractItemModel) and the QObject signal/slots mechanism
to react to user interactions. So nothing really new from this side. There
stuff are getting different is related to graphics.

QtQuick uses QML as it primarely way to design an user interface. It is a
really nice language and it's really easy to interact between QML and C++.