---
title: MarkNote 1.2
date: "2024-05-17T16:15:35Z"
hideHeaderImage: true
categories:
  - Free Software
  - KDE
tags:
  - KDE
image: sketches.png
---

The MarkNote team is happy to announce the 1.2 release of MarkNote, KDE's
WYSIWYG note-taking application. Marknote lets you create rich text notes and
easily organise them into notebooks. You can personalise your notebooks by
choosing an icon and accent color for each one, making it easy to distinguish
between them and keep your notes at your fingertips. Your notes are saved as
Markdown files in your Documents folder, making it easy to use your notes
outside of Marknote as well as inside the app.

## Notes management

This releases brings highly wanted features like the ability to choose a custom
folder where to store your notes. Mathis Brüchert also added the ability to
change the sorting of notes from alphabetically to by date.

Mathis made the sidebar collapsable and added a focus mode where everything but
the editing page is displayed.

<figure style="max-width: 100%; margin-left: auto; margin-right: auto;" class="embed-responsive embed-responsive-16by9">
  <video src="sidebar.mp4" loop autoplay muted></video>
</figure>

Finally if you prefer to just use Marknote as a Markdown editor, we made it
possible to just open any markdown files directly from the file browser or the
console. Additionally Marknote supports markdown files with a so called front
matter, which is a common way to inject metadata to markdown in static website
generators like Hugo and Jekyll.

## Editing

In term of edition support, the subset of markdown supported increased again.
Now it is possible to add and edit tables.

![](table.png)

Additionally we started transforming inline markdown directly to rich text as
you type. Support is limited to a few markdown constructs but is likely to
grow over time.

<figure style="max-width: 100%; margin-left: auto; margin-right: auto;">
  <video style="box-shadow: var(--shadow-l4); border-radius: 5px;" src="transform.mp4" loop autoplay muted></video>
</figure>

You can now customize the font used by editor.

![](settings.png)

Aside from being able to edit text, it's now possible to also create sketches
directly from MarkNote.

![](sketches.png)

## Mobile Support

Mathis took care of ensuring MarkNote was fully usable when used with Plasma Mobile.

<figure style="max-width: 100%; margin-left: auto; margin-right: auto;">
  <video style="box-shadow: var(--shadow-l4); border-radius: 5px; " src="mobile.mp4" loop autoplay muted></video>
</figure>

## Windows and macOS support

Marknote now provides nightly builds for Windows and macOS. While the Windows
builds should be fully usable, the macOS build still has an issue where most
icons are not displayed. This should be fixed as soon as we can make use of
the new KIconTheme version.

As part of the work to improve the macOS support, Marknote also gained global
menu support for Linux.

## Others

* The command bar will show translated shortcuts. (Laurent Montel)
* Unify spelling of MarkNote and fix typos in the README.md (Jonah Brüchert)

## Get Involved

Mathis created a Matrix channel for MarkNote:
[#marknote:kde.org](https://go.kde.org/matrix/#/#marknote:kde.org). There is
also still a lot of small improvements that can be done everywhere and which
don't require a lot of programming experience. Take a look at these two tasks
[!31](https://invent.kde.org/office/marknote/-/issues/31) and
[!27](https://invent.kde.org/office/marknote/-/issues/27) for some inspiration
on what you could work on.