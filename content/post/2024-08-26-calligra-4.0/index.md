---
title: Calligra Office 4.0 is Out!
date: 2024-08-27
draft: true
---

Calligra is the office and graphics suite developed by KDE and is the successor
to KOffice. With some traditional parts like Kexi and Plan having an
independent release schedule, this release only contains the four following
components:

- [Calligra Words](https://calligra.org/words/):  Word Processor
- [Calligra Sheets](https://calligra.org/sheets/): Spreadsheet Application
- [Calligra Stage](https://calligra.org/stage/): Presentation Application
- [Karbon](https://calligra.org/karbon): Vector Graphics Editor

The most significant updates are that Calligra has been fully transitioned to
Qt6 and KF6, along with a major overhaul of its user interface.

## General

Words, Sheets, and Stage now feature a new sidebar design. Currently, this is
implemented using a proxy style, which will no longer be necessary once the
related merge request in Breeze is
[reviewed and integrated](https://invent.kde.org/plasma/breeze/-/merge_requests/478).

![Sidebar with the new immutable tab design](sidebar.png)

I revamped the content of each sidebar tab, addressing various visual glitches
and making the spacing much more consistent.

The "Custom Shape" docker has been removed, and custom shapes are now
accessible through a popup menu in the toolbar across all Calligra
applications.

![Custom shapes popup](customshapes.png)

Regarding the toolbar, I streamlined the default layout by removing basic
actions like copy, cut, and paste.

The settings dialogs were also cleaned up and are now using the new FlatList
style also used by System Settings and most Kirigami applications.

![Settings Dialog](dialogs2.png)

## Words

Word now features the new sidebar design, and the main view uses a shadow to
define the document borders.

![Calligra Words](words.png)

The _Style Manager_ and _Page Layout_ dialog were also updated.

![Style Manager](stylemanager.png)

![Page Layout](pagelayout.png)

## Stage

Stage didn't really change aside of the sidebar redesign. But I am using it to work
on my slides for Akademy and it is a pretty solid choice.

![Calligra Stage](stage.png)

The tooltip for the slides are now compatible with Wayland.

![Tooltip showing a slide](tooltip.png)

## Calligra Sheets

As part of the Qt6 port, Sheets lost its scripting system based on the
unmaintained Kross framework. In the future, it would be possible to add Python
scriping, thanks to the work of [Manuel Alcaraz
Zambrano](https://invent.kde.org/manuelal) on getting Python bindings for the
KDE Frameworks.

Visually a noticable change is that the cell editor moved from a docker
positioned on the left of the spreadsheet view by default to a normal widget on
the top. This takes a lot less space which can be used by the spreadsheet.

![Calligra Sheets](sheets.png)

## Karbon

Karbon didn't received much change outside of the one affecting the whole
platform.

![Karbon](karbon.png)

## Launcher

The intial window when opening one of the Calligra application was redesign and
adopted the new "[frameless style](/2023/08/29/frameless-view-with-qtwidgets/)".

![Custom Document tab of the launcher page](open.png)

![Template tab of the launcher page](open2.png)

## Other

* [Braindump](https://calligra.org/old-components/#braindump) is now able to
  compile again, but since it lacks an active maintainer, the component is
  disabled in release builds.

* The webshape plugin has been ported from the outdated QtWebkit module to
  QtWebEngine and is no longer exclusive to Braindump. This means you can now
  embed websites directly into your word documents, slides, and spreadsheets.

![Webshape](webshape.png)

* The AppStream id of every components is prefixed by `org.kde.calligra`. This
  allow Flatpak to expose every Calligra applications to your application
  launcher.

## Get Involved

Calligra needs your support! You can contribute by getting involved in
development, providing new or updated templates, or making a donation to [KDE
e.V.](https://kde.org/donate/). Join the discussion in our [Matrix
channel](https://go.kde.org/matrix/#/#calligra:kde.org).

## Credits

This release would not have been possible without the high quality
[mockups](https://phabricator.kde.org/T12837) provided by [Manuel Jesús de la
Fuente](https://mastodon.social/@manueljlin). Also big thanks to everyone who
contributed to this Calligra release: Evgeniy Harchenko, Dmitrii Fomchenkov and
bob sayshilol.