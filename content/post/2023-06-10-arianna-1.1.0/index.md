---
title: Announcing Arianna 1.1
aliases:
 - /2023/06/10/announcing-arianna-1.0/
date: "2023-06-10T11:00:00Z"
hideHeaderImage: true
categories:
  - Free Software
  - KDE
  - Arianna
tags:
  - Arianna
  - KDE
image: table-of-content.png
comments:
  host: kde.social
  username: arianna
  id: 110519744928750685
---

I'm happy to announce the 1.1 release of Arianna. Arianna is a small ePub
reader application I started with Niccolo some time ago. Like most of my open
source applications, it is built on top of Qt and Kirigami.

Arianna is both an ePub viewer and a library management app. Internally,
Arianna uses Baloo to find your existing ePub files in your device and
categorize them.

## New features

Arianna can now display the table of content of a book. This supports complex
hierarchies of headings.

![A table of content displayed as a right sidebar with a tree structure](table-of-content.png)

Arianna now provides you with the metadata about your books.

![Dialog showing the title, author, description, license information about a book](book-detail.png)

Additionally, you can now disable the reading progress on the library page if
it distracts you.

## Bug fixes

You can now read books without requiring an internet connection. We also fixed
various crashes happening when indexing your books.

## Get Involved

If you are interested in helping, don't hesitate to reach out in the Arianna
matrix channel ([#arianna:kde.org](https://matrix.to/#/#arianna:kde.org)) and I
will be happy to guide you.

I also regularly post about my progress on Arianna (and other KDE apps) on my
[Mastodon account](https://floss.social/@carlschwan), so don't hesitate to
follow me there ;) We also now have an official Mastodon account for Arianna
[@arianna@kde.social](https://kde.social/@arianna).

And in case you missed it, as a member of KDE's fundraising working group, I
need to remind you that KDE e.V., the non-profit behind the KDE community
accepts [donations](https://kde.org/fundraisers/yearend2022/).

## Packager section

You can find the package on
[download.kde.org](https://download.kde.org/stable/arianna/arianna-1.1.0.tar.xz.mirrorlist)
and it has been signed with my [GPG key](/gpg-02325448204e452a/).