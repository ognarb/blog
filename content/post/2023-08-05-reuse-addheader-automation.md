---
title: Reuse annotate automation
date: "2023-08-05T17:00:00Z"
hideHeaderImage: true
categories:
  - Free Software
---

I <s>tooted</s> posted [on Mastodon](https://floss.social/@carlschwan/110837388714548838)
about how it would be great if more open source project would adopt REUSE.

Someone pointed out that it would be great to automate the addition of the metadata inside the files based on the git repository.

So here is a small script that does exactly that. It goes over all your .cpp and .h file and will add the header based on the list of authors as well as the first commit on that particular file.

You will want to adapt it to the license you are using for your project as well as change `**/*.cpp` to `**/*.py` if you are doing Python development.

```bash
#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2023 Carl Schwan <carl@carlschwan.eu>
# SPDX-License-Identifier: MIT

for file in **/*.cpp **/*.h;
do
    year=`git log --format="format:%ci" --reverse $file | head -n1 | cut -d "-" -f 1`
    git shortlog -n -e -s -- $file | cut -f 2 | while IFS= read -r author
    do
        reuse annotate --copyright "$author" --year "$year" --license "GPL-2.0-or-later" $file
    done
done
```

This is not perfect and you want to run `git diff` before commiting the result, to check if the change made are fine.