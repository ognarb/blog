---
title: KDE PIM in May and June
date: "2022-07-04T12:00:35Z"
image: /2022/06/14/kalendar-contact-book-current-progress/contact-view.png
hideHeaderImage: true

certificates:
- url: '2022-0506-Certificate Creation - Step 1.png'
  name: 'Certificate Creation - Step 1' 
- url: '2022-0506-Certificate Creation - Step 2.png'
  name: 'Certificate Creation - Step 2' 
- url: '2022-0506-Certificate Creation - Step 3.png'
  name: 'Certificate Creation - Step 3' 

qt6:
- url: 'qt6.png'
  name: "KAdressBook with Qt6"
- url: 'akregator-qt6.png'
  name: "AKregator with Qt6"
- url: 'kleopatra-qt6.png'
  name: "Kleopatra with Qt6"
- url: 'sieveeditor-qt6.png'
  name: "Sieve Editor with Qt6"

kalendar:
- url: /2022/06/14/kalendar-contact-book-current-progress/contact-view.png
  name: Contact Viewer
- url: /2022/06/14/kalendar-contact-book-current-progress/search-applet.png
  name: Plasma Applet searching for a contact
- url: /2022/06/14/kalendar-contact-book-current-progress/applet.png
  name: Plasma Applet
- url: /2022/06/14/kalendar-contact-book-current-progress/contact-editor.png
  name: Editor
---

KDE PIM is the set of applications that helps you manage your email, contacts,
appointments, tasks and more.

In the months since the [KDE PIM March-April report](https://volkerkrause.eu/2021/05/01/kde-pim-march-april-2021.html)
there have been two patch releases for Kontact, and over 1300 changes made by
more than 30 contributors have been integrated. Here are some of the highlights.

## General Improvements

Laurent continued working on the Qt6 support. The KDE PIM packages are now compiling
with Qt6.

{{< screenshots name="qt6" >}}

## Kleopatra

Ingo worked mainly on improving the usability and accessibility of the certificate manager
[Kleopatra](https://apps.kde.org/kleopatra) over the last two months:

- Generating a new OpenPGP certificate is now possible on a Full HD display with 400
% magnification. ([T5969](https://dev.gnupg.org/T5969))
- Generating a new OpenPGP certificate has been made more accessible by replacing
the QWizard-based dialog with a few separate dialogs. ([T5832](https://dev.gnupg.org/T5832))

{{< screenshots name="certificates" >}}

- The tool bar in the main window is now accessible with the keyboard. ([T6026](https://dev.gnupg.org/T6026))
- Links embedded in text labels now behave like normal links you would find on the
web. This means that accessibility tools (such as screen readers) will read them as such,
instead of like selected text. ([T6034](https://dev.gnupg.org/T6034))
- Labels that receive the keyboard focus are now marked with a focus frame. ([T6036](https://dev.gnupg.org/T6036))
- The accessibility of the Certificate Details dialog has been improved. ([T5843](https://dev.gnupg.org/T5843))

Moreover, new features were added:

- The Certificate Details dialog now allows refreshing an individual certificate from
the configured keyserver and via Web Key Directory. ([T5903](https://dev.gnupg.org/T5903))<br>

![Certificate Details dialog with an accessible focus indicator](2022-0506-Certificate-Details.png)

- Administrators can now specify the minimum and maximum validity of newly generated
keys. ([T5864](https://dev.gnupg.org/T5864))

And a few smaller things were added or fixed:

- A few operations that are not possible for keys stored on smart cards, e.g. creating a
backup of the secret key or changing the passphrase used to protect the secret key, are
now disabled if a smart card key is selected to avoid weird error messages.
([T5956](https://dev.gnupg.org/T5956), [T5958](https://dev.gnupg.org/T5958))
- Felix Tiede added a feature to publish a GPG key at a WKS-enabled mail
provider.

As this requires Kleopatra to operate with Akonadi to evaluate a mail-
transport for a GPG key user id and then send a MIME mail message, this
feature is only available on systems where Akonadi is installed prior to
building Kleopatra. On all other systems it is unavailable.

## Kontact

Glen fixed a few issues in the settings for the Summary View of Kontact:

- The Special Dates section obeys the check boxes for showing birthdays
and wedding anniversaries of contacts, and for showing anniversaries in
other calendars.
- The check box for hiding open-ended to-dos in the Pending To-dos
section is checked correctly.

## KOrganizer

Glen also worked on the task view of KOrganizer.

- Right-clicking on a task's start date now displays a date editor
widget which you can use to change the start date. Right-clicking the
due date still edits the due date. If a change to either date would
cause the start date to be later than the due date, the other date is
adjusted. Additionally marking a task as complete in the Summary View
of Kontact, will no longer cause a crash.
- KOrganizer now uses the "stand-alone" form of month names in appropriate
places ("June", as opposed to "June 1"). In some languages the two forms
are different.
- In the Incidence Editor, unchecking the "Due" check box of a recurring
to-do now accomplishes something.
- If a yearly-recurring item has exceptions, the Item Viewer lists the
exceptional years.

## KMail

- Laurent fixed a few memory leaks in KMail and he started working on integrating
[Message Disposition Notification (MDN)](https://datatracker.ietf.org/doc/html/rfc8098)
directly into the viewer instead of in a separate dialog. This will allow sending
read receipts more easily.
- He also fixed marking more than 1000 emails as read at the same time
([BUG 453969](https://bugs.kde.org/453969)).
- Sandro Knauß worked on refactoring the key expiry checker in preparation for
showing it in a non-blocking dialog in the future.
- They also made it possible to
overwrite the encoding of the PGP inline messages.

## Pim-data-exporter

- The data importer and exporter received a lot of bug fixes that will
improve the import of maildir resource and the collectio attribute.

## Calendaring

There are now Akonadi and Android plugins for KCalendarCore's platform
calendar access API. This isn't aimed at calendaring applications like
Kalendar or KOrganizer, but at apps for which just basic calendaring access is
required. See the separate blog post for details:
[Android Platform Calendar Access](https://volkerkrause.eu/2022/06/18/kf5-android-platform-calendar-access.html).

## KDE Itinerary

Please see the dedicated summary blog post: [KDE Itinerary April May](https://volkerkrause.eu/2022/06/04/kde-itinerary-april-may-2022.html)

## KAlarm

- David Jarvie fixed a crash in the font chooser after deselecting a default font
([BUG 453193](https://bugs.kde.org/453193))
- The fade controls won't be displayed if the current phonon backend
doesn't support fade. And sound files are now correctly played when played
previously with fade.

## Kalendar

- Carl worked on an address book integration for Kalendar. This now includes a contact
viewer that more or less displays all the contact information, a basic contact
editor and also handling for contact groups.
- Also included is a Plasma applet and QR code for sharing contacts. More on this can be read
on my previous [blog post](https://carlschwan.eu/2022/06/14/kalendar-contact-book-current-progress/).

{{< screenshots name="kalendar" >}}

- Claudio made it possible to show the parent and sub-tasks in the incidence drawer.
This allows you to navigate between related tasks in the normal calendar views,
and not only the task view.

![Kalendar task](kalendar-tasks.png)

- Claudio also put a lot of effort into reducing the technical debt in
Kalendar. He simplified the model for the month view, reorganized our QML files
into subfolders. This should help us when adding more features in the future.

## Help us make Kontact even better!

Check out some of our open junior jobs! They are simple, mostly programming-focused
tasks, but they don’t require any deep knowledge or understanding of Kontact, so
anyone can work on them. Feel free to pick any task from the list, then get in touch
with us! We’ll be happy to guide you and answer all your questions.
[Read more here...](https://www.dvratil.cz/2018/08/kde-pim-junior-jobs-are-opened/)

{{< swiper >}}
