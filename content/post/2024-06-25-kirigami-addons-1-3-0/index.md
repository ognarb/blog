---
title: Kirigami Addons 1.3.0
date: "2024-06-26T11:20:35Z"
categories:
  - Free Software
  - KDE
tags:
  - KDE
  - Kirigami Addons
hideHeaderImage: true
image: configurationview-desktop.png
comments:
  host: floss.social
  username: carlschwan
  id: 112682688187568487
---

Kirigami Addons 1.3.0 is out. Kirigami Addons is a collection of components to
enhance your Kirigami/QML application. This release contains many change
related to the settings module.

## ConfigurationView

The current way to create a settings page in your application is to use
`CategorizedSettings` with some `SettingAction` for each setting page. This was
based on `Kirigami.PageRow` which was then either pushed on a layer on mobile
or to a seperate page on desktop. This turned out to be quite unreliable in
practice as `Kirigami.PageRow` is a visual element.

The new `ConfigurationView` is based on a plain non-visual `QtObject` with for
the moment two backends:

- One for mobile which looks similar to the Plasma Settings application of
  Plasma Mobile.
- One for desktop which looks similar to the System Settings application of
  Plasma Desktop.

The API is almost the same as the previous CategorizedSettings which made
porting quite easy. Here is for example a button that open the settings.

```qml
import QtQuick.Controls as Controls
import org.kde.kirigamiaddons.settings as KirigamiSettings

Controls.Button {
    id: button

    KirigamiSettings.ConfigurationView {
        id: configuration

        window: button.Controls.ApplicationWindow.window as Kirigami.ApplicationWindow

        modules: [
            KirigamiSettings.ConfigurationModule {
                moduleId: "appearance"
                text: i18nc("@action:button", "Appearance")
                icon.name: "preferences-desktop-theme-global"
                page: () => Qt.createComponent("org.kde.tokodon", "AppearancePage")
            },
            ...
            KirigamiSettings.ConfigurationModule {
                moduleId: "about"
                text: i18nc("@action:button", "About Tokodon")
                icon.name: "help-about"
                page: () => Qt.createComponent("org.kde.kirigamiaddons.formcard", "AboutPage")
                category: i18nc("@title:group", "About")
            }
        ]
    }

    icon.name: 'settings-configure-symbolic'
    text: i18nc("@action:button", "Settings")

    onClicked: configuration.open()
}
```

With this change, both `CategorizedSettings` and `SettingAction` are now deprecated.

![ConfigurationView on desktop](configurationview-desktop.png)

![ConfigurationView on mobile](configurationview-mobile.png)

## SpellcheckingConfigurationModule

With `ConfigurationView` each page is a `ConfigurationModule` and Kirigami
Addons provides a `ConfigurationModule` for the spellchecking configuration of
your application. This will allow to reduce code duplication between NeoChat,
Tokodon, Marknote and more applications which uses Sonnet.

```qml
KirigamiSettings.ConfigurationView {
    modules: [
        KirigamiSettings.SpellcheckingConfigurationView {}
    ]
}
```

## FormCard

FormCard design was slighly updated and now uses shadows as you might have
already noticed from the previous screenshots.

## SearchPopupField

Another component which is getting deprecated in `SearchPopupField`, there is
now a replacement for that in Kirigami with the same behavior and I added an
example how to port away to `Kirigami.SearchDialog` and I also ported all the
know usage already.

## Maintainance work

Aside from this major changes, there is ongoing maintaince works. This includes:

- Removing the accidental QtWidgets on Android caused by QtLabs.ColorDialog (me: Carl Schwan)
- Ensure all translated strings are loaded from the correct translation domain (me: Carl Schwan)
- The license dialog in the AboutPage is now opened in the correct window (Jack Hill)
- Fix the focus in the FormComboBoxDelegate (Joshua Goins)
- Fix the capitalization in the AboutPage (Joshua Goins)
- Increase the padding in FormCardDialog to match the other FormCard components

## Packager Section

You can find the package on
[download.kde.org](https://download.kde.org/stable/kirigami-addons/kirigami-addons-1.3.0.tar.xz.sig.mirrorlist)
and it has been signed with my [GPG key](/gpg-02325448204e452a/).