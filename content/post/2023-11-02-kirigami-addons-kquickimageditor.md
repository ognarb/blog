---
title: KQuickImageEditor 0.3.0 and Kirigami Adddons 0.12 alpha
date: "2023-11-02T16:00:00Z"
---

Two new releases are out in preparation of the first alpha release or the February
Megarelease.

## KQuickImageEditor 0.3.0

This is the new stable version of KQuickImageEditor. Only notable change it the
support for Qt6 in addition of Qt5 support.

## Kirigami Addons 0.11.75

This is an alpha release and depends on an unreleased KDE Frameworks. Please
only package it if you also package the coming alpha megarelease.

## Packager section

You can find the package on
[download.kde.org (kirigami addons)](https://download.kde.org/unstable/kirigami-addons/kirigami-addons-0.11.75.tar.xz.mirrorlist) and [download.kde.org (kquickimageeditor)](https://download.kde.org/stable/kquickimageeditor/kquickimageeditor-0.3.0.tar.xz.mirrorlist)
and it has been signed with my [GPG key](/gpg-02325448204e452a/).