---
title: Kirigami Addons 1.0
date: "2024-02-22T10:00:00Z"
hideHeaderImage: true
categories:
  - Free Software
  - KDE
tags:
  - KDE
image: formcard.png
---

A new version of Kirigami Addons is out! Kirigami Addons is a collection of
helpful components for your QML and Kirigami applications. With the 1.0
release, we are now supporting Qt6 and KF6 and added a bunch of new components
and fixed various accessibility issues.

## FormCard

We added a bunch of new FormCard delegates:

- `FormPasswordFieldDelegate`: A password field
- `FormDataTimeDelegate`: A date and/or time delegate with integrated date and
  time picker which use the native picker of the platform if available
  (currently only on Android).

![Form card example](formcard.png)

The existing delegates also recevied various accessibility issues when used
with a screen reader.

Finally we droped the compatibility alias `MobileForm`.

## BottomDrawer

Mathis added a new Drawer component that can be used a context menu or to
display some information on mobile.

![Bottom Drawer in Itinerary showing the information about a station on the map](bottomdrawer.png)

## FloatingButton and DoubleFloatingButton

These two components received significant sizing and consistency improvements
which should improve their touch area on mobile.

## Packager section

You can find the package on
[download.kde.org](https://download.kde.org/stable/kirigami-addons/kirigami-addons-1.0.0.tar.xz.mirrorlist)
and it has been signed with my [GPG key](/gpg-02325448204e452a/).