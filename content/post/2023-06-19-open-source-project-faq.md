---
title: My generic Open Source Project FAQ
date: "2023-06-19T8:30:00Z"
categories:
  - Free Software
  - KDE
tags:
  - KDE
comments:
  host: floss.social
  username: carlschwan
  id: 110569991564308866
---

People are often asking the same questions again and again about some of my
projects, so it might be a good opportunity to write a small FAQ.

If you get redirected here, don’t take it personally; I am getting asked these
questions very often, and I feel people often misunderstand how open-source
projects work.

## Why does X not have feature Y?

The most likely reason is that it still needs to be implemented. It doesn’t
mean that I or other maintainers are against this feature. It is just that X is
a purely non-commercial project, and I and others are currently working on it
during our free time. Unfortunately, Free time is a very limited and precious
resource. Between our day jobs or university projects, sleeping, eating, and
other social activities, little time and energy is left.

We definitively might implement the feature in the future, but the best way to
ensure this gets implemented promptly is to get involved and implement it
yourself. Feel free to join our development channel beforehand and confirm that
the feature is something we would like to have in the application.

We are happy to guide you to make the onboarding experience as good as possible
and to help you familiarize yourself with the code base. Getting involved with
open-source projects is also an excellent opportunity to learn how to program.
Check out the [Google Summer of Code](https://summerofcode.withgoogle.com/),
[Outreachy](https://www.outreachy.org/), or
[Season of KDE](https://season.kde.org/) for special mentoring programs, but
getting involved outside these programs is also possible. On a personal note, I
learned most of my programming skills by contributing to KDE, so I am very
thankful for that.

## When will you implement feature Y? Is Y on your roadmap?

Similarly to the previous question, X is a non-commercial project implemented
during my and others’ free time. We can’t say precisely when we will be done
with a particular feature as it depends on many factors: how much energy do we
currently have to work on this project after our day job/university projects,
are there more pressing issues, are there a technical blocker which needs to be
solved first, it is fun to implement…

Again the best way to speed this up is to get involved.

## What is your roadmap?

We are a non-commercial project which is purely volunteer based. We are just a
bunch of developers and designers; we do not have managers, stakeholders, or
clients influencing our work directly by setting deadlines or by asking **and
paying** for specific features. Part of the reason we enjoy working on this
project is that we have a lot of freedom in terms of what we want to work on
and are also very flexible, allowing us to switch to a different task whenever
we want.

Again the best way to influence the direction of this project is to get
involved.
