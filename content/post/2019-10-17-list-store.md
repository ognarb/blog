---
date: "2019-10-17T20:35:35Z"
title: List stores in kde.org/applications
---

Announcing a small update for the goal: KDE is All About the Apps.
[kde.org/applications](https://kde.org/applications) is now listing
the stores where the application is available. For the moment, it's only
listing Linux and the Windows Store, but support for F-Droid and the
Play Store is planned. Stay tuned!

<img alt="Store listing" src="/assets/img/store-listing.png" style="border: 1px solid black; max-width: 100%"/>
