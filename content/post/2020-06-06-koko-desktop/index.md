---
date: "2020-06-06T13:50:00Z"
locale: en
title: Making Koko Great for Desktop Use
image: koko.png
---

Koko is a great image viewer designed for Plasma Mobile. Unfortunately, up until recently it did not work great on desktop computers. This changed last week with various small patches to Koko's code and Kirigami, KDE's framework for developing apps for the desktop and mobile devices which is what Koko is based on.

First the sidebar color was updated to use the view colorset instead of the window colorset. This change was initially only done in Koko but, seeing that various Kirigami apps had this configuration too, I updated the default in Kirigami so that each app doesn't have to change it again. ([Kirigami !15](https://invent.kde.org/frameworks/kirigami/-/merge_requests/15))

I added a proper header to the sidebar describing what the sidebar does and also made a small change to the list header. Now, if all the actions list are expandable, the list header look is applied to them. This makes it more similar to the sidebar on System Settings. That said, more work is needed to achieve a consistent look. ([Kirigami !19](https://invent.kde.org/frameworks/kirigami/-/merge_requests/19))

I added "next" and "previous" buttons on the desktop mode to the image viewer itself, as swipe gestures are harder to use on desktop apps. ([Koko !3](https://invent.kde.org/plasma-mobile/koko/-/merge_requests/3))
 I also added text to the context buttons to make it clearer what the actions do. ([Koko !2](https://invent.kde.org/plasma-mobile/koko/-/merge_requests/2))

Another thing I improved was the animation for showing or hiding the thumbnail list: It doesn't steal the scrollbar input anymore. ([Koko !12](https://invent.kde.org/plasma-mobile/koko/-/merge_requests/12))

<video src="/assets/img/koko-animation.mp4" autoplay muted></video>

A more exciting feature that hasn't landed yet is that I am including a simple image editor for upcoming versions. The image editor is developed as an external library that can be embedded into Koko and also into a future version of spectacle for plasma mobile, Pix, or QML messaging apps. It can also be included into messenger apps and possibly more types of apps. The API is not stable yet, so if you are interested in integrating a basic image editor into your qml app, please let me know! The code is available at [invent.kde.org/libraries/kquickimageeditor](https://invent.kde.org/libraries/kquickimageeditor) and merge request and features suggestions are welcome.

<video src="/assets/img/koko-editor.mp4" autoplay muted></video>

Is Koko already a full replacement for Gwenview? Probably not or at least not for yet for everyone. But with a bit more love it could become a serious contender.
