---
title: Francis 1.0
date: "2023-09-08T08:00:00Z"
hideHeaderImage: true
categories:
  - Free Software
  - KDE
  - Kirigami
tags:
  - KDE
  - Kirigami
image: francis.png
---

![Francis logo](francis-logo.png)

Today is my birthday but it's also the day Francis got its first release.
Francis is a pomodoro app, which was originally developed by Felipe Kinoshita.
The Pomodoro Technique is a time management method developed by Francesco
Cirillo in the late 1980s. It uses a kitchen timer to break work into
intervals, typically 25 minutes in length, separated by short breaks.

![Francis screnshot](francis.png)

The app is very simple and can be used as inspiration to develop your own
Kirigami application.

## Get Involved

If you are interested in helping, don't hesitate to reach out in the Plasma Mobile
matrix channel ([#plasma-mobile:kde.org](https://matrix.to/#/#plasma-mobile:kde.org)) and I
will be happy to guide you.

I also regularly post about my progress on many KDE apps on my
[Mastodon account](https://floss.social/@carlschwan), so don't hesitate to
follow me there ;)

And in case you missed it, as a member of KDE's fundraising working group, I
need to remind you that KDE e.V., the non-profit behind the KDE community
accepts [donations](https://kde.org/fundraisers/yearend2022/).

## Packager section

You can find the package on
[download.kde.org](https://download.kde.org/stable/francis/francis-1.0.0.tar.xz.mirrorlist)
and it has been signed with my [GPG key](/gpg-02325448204e452a/).