---
title: Independent releases
date: "2024-02-28"
draft: true
---

Following the KDE MegaRelease containing a massive amount of changes to Plasma
and Gear applications, here is a few more releases.

