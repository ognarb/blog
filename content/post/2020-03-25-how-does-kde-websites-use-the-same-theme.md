---
date: "2020-03-25T12:50:00Z"
post: layout
render_with_liquid: false
title: Why are most KDE websites using the same theme?
---

## A unified theme? Why? Why not?

Nearly all KDE websites use a unified theme across the board. This is part of the consistency goal, chosen as a KDE goal at the last Akademy in Milano (Italy).

Using a unified theme has multiple advantages:

* It strengthens KDE's brand, since websites are more visible than applications or projects part of the KDE community. Websites with a unified look help visitors immediately identify an application or subproject as being part of a bigger organization.

* It is easier to maintain only one theme and the tooling behind it than a theme for each project. We all wish Free and Open Source projects had unlimited budgets and manpower, but that is not realistic and maintaining a theme is not an easy task.

This doesn't mean the KDE theme should be used for every KDE project, in fact, there are legitimate reasons not to. Sometimes an application under the KDE umbrella wants to create their own brand and does have the manpower and talent to design their own website. [Krita](https://krita.org) is a good example here. The team has built a beautiful website that does not use the unified theme.

## What theme does KDE use?

KDE is using the Aether theme. This is a theme designed and initially developed by [Ken Vermette](https://kver.wordpress.com), the talented artist that is also behind most of the Plasma wallpapers and some interesting design concept like [DWD](https://kver.wordpress.com/2014/10/25/presenting-dwd-a-candidate-for-kde-window-decorations/).

This theme was originally based on one of the first Bootstrap 4 alpha version and later rebased on a stable Bootstrap 4 version. Using Bootstrap has its advantages and disadvantages.

The biggest advantage is that it has a large community and a lot of bootstrap themes exist for CMS and static site generators. It can be easily adapted to your specific needs without starting from scratch every time.

Another advantage is that Bootstrap is built using SASS and is designed to be extendable with tons of variables a developer can modify to globally change colors, layouts and a lot more. You can also specify the modules you want to use, and add your self-made components. For those interested in extending a Bootstrap theme, the [official documentation](https://getbootstrap.com/docs/4.4/getting-started/theming/) is a great start. These capabilities were sadly not used when creating the Aether theme, but we are slowly moving to use more of the Bootstrap theming capabilities over time.


The problem with Boostrap is that, because it is so popular, in its default from it looks like a generic website without any personal identity. Changing only the colors won't help to make your website more unique.

## Building the websites

In KDE we use a lot of different web frameworks for our websites. Most of the applications websites are now using Jekyll or Hugo, but some are also using Wordpress, Drupal 7 and Drupal 8.

Jekyll has first-class support for creating a shared theme, so we created a Jekyll theme for KDE using the Aether theme that offers a lot of options. Here are two examples:

* Should it display a Made By KDE logo? This is useful for application websites but shouldn't appear on, for example, the KDE e.V. website.

* Should it display a KDE logo or the application logo?

This allows for small customizations with only a few changes for each website without having to maintain different versions.

Some websites are using Hugo instead of Jekyll. The reason for this is that Hugo is faster when generating the webpages and has more advanced internationalization features included by default. The internationalization features are used for [kate-editor.org](https://kate-editor.org) and the [monthly application release announcements](https://kde.org/announcements/releases).

For my WIP replacement for the [KDE Dot](https://dot.kde.org), I needed the performance of Hugo. With Jekyll, I gave up generating the website after 10 minutes, but with Hugo, it only took a few seconds to generate.

In the case of Content Management Systems, creating a theme is also well-supported but it isn't as
pleasant to do. I have a limited experience with Wordpress, but creating a theme was still possible and not too complicated. In the case of Drupal 7, creating a theme from scratch is almost impossible. It requires messing with a lot of PHP arrays and the security responsibility is on the side of the theme developer.

## Deployment of the theme

To deploy the theme, I use [Symfony Encore](https://symfony.com/doc/current/frontend.html). Sympfony Encore is a [webpack](https://webpack.js.org/) wrapper. I chose Symfony Encore because I'm very familiar with its PHP framework and I have been working with it since I was in high school (using Webpack alone would have been completely fine too).

Webpack is used to compile and optimize the SCSS and ECMAScript (aka JavaScript). After each commit to the master branch, a CI job compiles and deploys the compiled CSS and ECMAScript to a CDN host and the hosted CDN asset is then used by all the new KDE websites. This means that my changes are immediately propagated to all websites... or so I thought initially. In reality the CSS and JS files are cached for a long time and often visitors get an old version.

I'm solving this problem in two ways:

* For dynamic websites: I also generate a versioned version of the assets and resolve the version in the backend. This is what I used in the
[MediaWiki plugin](https://invent.kde.org/websites/aether-mediawiki/-/blob/master/SkinAether.php), for example.

* For static websites: In this case, I can't dynamically resolve the last version in the backend, so instead I add a GET parameter to the URL (e.g. main.css?v=10) and increase the number each time I fix a bug. This doesn't happen often, so it is still easy to manage.

## How you can help

We always need help with the websites, fixing papercuts, upgrading old websites to the new Jekyll/Hugo infrastructure, making sure information on the website is up-to-date, creating new beautiful home pages for your favorite projects and a lot more.

The Elisa maintainers are still looking for someone who wants to create a website and we created a [junior job task](https://phabricator.kde.org/T12726) for this. Maybe that someone could be you?

You can also join the web team through our [Matrix channel](https://webchat.kde.org/#/room/#freenode_#kde-www:matrix.org), our IRC channel (#kde-www) or our [Telegram channel](https://t.me/KDEWeb).
