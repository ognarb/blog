---
title: Initial work on Keychain
date: "2024-07-01T00:00:00Z"
categories:
  - Free Software
  - KDE
tags:
  - KDE
  - Keychain
hideHeaderImage: true
image: mainview.png
comments:
  host: floss.social
  username: carlschwan
  id: 112711196518031743
---

A month ago, I started working on a new application to manage your passwords in
Plasma. And while still at a PoC status, this weekend, it finally started to
look like something almost usable, so it sounded like a good occassion to write
a small blog post about it.

The current name is "Keychain" or "Plasma Keychain" but this is subject to
change and suggestions are more than welcome.

My end goal is to provide a more future proof replacement to the ageing KWallet
application. From a technical point of view, this is a fork of the internal of
KeepassXC with a Kirigami GUI completely written from scratch. This means it
uses the standardized Keepass format to store the passwords in the database
which is implemented by many applications including on other platforms like Android and
iOS (see the list of [Keepass port](https://keepass.info/download.html)). And
while not yet exposed in the GUI, basing the work on top of KeepassXC enables a
lot of interesting features not available in KWallet, like Yubikey and PassKey
support, password sharing, export and import for various other password
database formats, TOTP support and browser integration...

While also providing vital features for the desktop integration like the
Freedesktop Secret Service protocol what we also have in KWallet.

Here are some screenshots of the current state.

This is the main view where viewing, adding, editing and removing entries
already work.

![Main View](mainview.png)

This is the database generator page which unfortunately doesn't work yet.

![Database generator](generate.png)

And this is the UI to open an existing database.

![Database generator](open.png)

As you can see there is still a lot of work required, so if people are
interested to help or to take a look at the current progress, the code is
[on KDE's gitlab instance](https://invent.kde.org/carlschwan/keychain).