---
title: "Tokodon 23.02.0 release"
date: "2023-02-17T12:00:00Z"
hideHeaderImage: true
categories:
  - Free Software
  - KDE
  - Tokodon
tags:
  - Tokodon
  - KDE
image: alt-text.png
comments:
  host: floss.social
  username: carlschwan
  id: 109879693388190620
---

I am happy to announce Tokodon 23.02. This release contains around one month's worth of improvements, and while it is only one month, this release is feature packed! This was an exciting month for me in general, I started working at [KDAB](https://www.kdab.com/), went to FOSDEM in Brusels where I meet a bunch of other KDE folks, and spend time enjoying my new life in Berlin.

![Me at the FOSDEM booth](fosdem.jpeg)

Fortunately I still found some time to contribute to Tokodon, and Joshua Goins has also been a very active contributor. The number of contributors is also growing with a bunch of new faces: Aakarsh MJ, Harshil Jani, Mohit Marathe, Raphael Gaschiganrd, Rishi Kumar who also contributed some code in this release.

So what is Tokodon? Is is basically a client for the federated social network [Mastodon](https://joinmastodon.org/). It is built with [Kirigami](https://develop.kde.org/frameworks/kirigami/) with the goal to have a great integration with Plasma Desktop and Plasma Mobile, but it also works on other desktop environments, and even Windows and macOS.

## Get it!

You can download Tokodon 23.02.0 from [Flathub](https://beta.flathub.org/apps/details/org.kde.tokodon) and from F-Droid using the [KDE repo](https://community.kde.org/Android/FDroid). Currently we don't offer binaries for Windows and macOS but if you want to help with that, please get in touch on [Matrix](https://matrix.to/#/#tokodon:kde.org).

## Post composer rewamp

As part of this release, we reworked the post composer considerably. This is the central area of your interaction with Mastodon and we wanted it to be more reliable.

It is now possible to add an alternative description to images and videos sent via Tokodon. This is very important for accessibility as images without alternative description are invisible for screen readers. (Carl Schwan, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/98))

![Alternative text](alt-text.png)

It is also now possible to see the upload progress when uploading an attachment, giving you some visual feedback that something is happening. (Carl Schwan, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/98))

An error message will be displayed when sending a message didn't work. The post composer will only be closed once the message is sent, so if there is an network error the post you wrote is not lost and you can try posting it again. This should make Tokodon much more reliable for interacting with Mastodon. (Joshua Goins, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/151)).

![Error displayed when an error occurred when sending a post](composer-error.png)

Another big thing is that we are now supporting editing posts, so in case you made a typo you don't need to delete your post and post it again. (Joshua Goins, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/145))

And finally Tokodon respects your default sensibility and visibility preferences when posting, and when replying to a message, the visibility of the message will be inherited in your reply. So if you are replying to a private message, the reply will automatically be set to be also private. (Carl Schwan, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/98))

## Timeline and notification view

Aside from the improvements to the post editor, we continued to polish the timeline.

The models for timelines has been completely refactored. Aside from making the code nicer to work with and a bit faster, it also made it possible to show the avatar of who boosted or liked a post. (Carl Schwan, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/134))

![Avatar of the user who boosted a post is now displayed](boost-avatar.png)

Tokodon now doesn't only support image as attachments, but we now also have a prelimary support for videos and gifs. (Joshua Goins, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/143)).

<div class="embed-responsive embed-responsive-16by9">
  <video src="video.mp4" muted="true" loop="true" autoplay></video>
</div>

Additionally the previews for attachements should be better displayed with correct aspect ratio. (Joshua Goins, [Link 1](https://invent.kde.org/network/tokodon/-/merge_requests/131), [Link 2](https://invent.kde.org/network/tokodon/-/merge_requests/131))

![New attachment layout](atttachments.png)

Other ways to interact with a post are now available in an overflow menu for posts adding actions like a "Copy link to post" feature (Joshua Goins, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/105)), a bookmark feature (Joshua Goins, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/109)) and the previously mentioned edit feature (Joshua Goins, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/145))

![Overflow menu showing additional actions](context-menu.png)

There are now some new special pages available in the sidebar to display your bookmarks as well as your favorite posts. (Joshua Goins, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/105))

![Favorite timelines](favoritetimeline.png)

We now handle notifications for poll updates (Riya Bisht, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/101)), and when a post you interacted with gets updated (Harshil Jani, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/97))

There are now placeholders for when you currently don't have any notifications or conversations instead of showing you an empty view (Mohit Marathe, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/148))

## Search

The search field introduced during the last released got a nice visual refresh...

![Search popup](searchpopup.png)

... And it now supports searching for hashtags. (Joshua Goins, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/138))

## Other Improvements

It is now possible to configure the color scheme used by Tokodon independently from the Plasma color scheme. (Joshua Goins, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/111) but most of the code was copied in good tradition from NeoChat)

![Color Scheme](colorscheme.png)

We added the missing separators in the settings pages (Rishi Kumar, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/123))

It is now possible to directly edit your account from your profile page (Joshua Goins, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/115))

The user's avatar in the profile page now uses the standard `Kirigami.Avatar` component (Joshua Goins, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/104))

The behavior of selectable text fields is now consistent (Aakarsh MJ, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/133))

## Important Bugfixes

A bug that broke loading the timeline in some conditions has been fixed (Carl Schwan, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/113))

We fixed rendering of some attachments caused by a bug in the blurhash decoding. (Joshua Goins, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/108))

Storing the account settings is now using the account username and instance hostname as a unique identifier. This fixes situations where someone had multiple accounts with the same username on a different instance. (Raphael Gaschiganrd, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/149))

## Technical Details

Aside from the many new features, there were several large re-factorings of the code base to cleanup some code that younger-me wrote. The C++ code base now has a coverage of 36%, this is a small increase compared to the previous release. Rishi Kumar is also working on integration tests as part of his Season of KDE project.

## Get Involved

If you are interested in helping, don't hesitate to reach out in the Tokodon matrix channel (#tokodon:kde.org) and I will be happy to guide you.

I'm also regularly posting about my progress on Tokodon (and other KDE apps) on my [Mastodon account](https://floss.social/@carlschwan), so don't hesitate to follow me there ;)

And in case, you missed it, as a member of the fundraising working group, I need to remind you that KDE e.V., the non-profit behind the KDE community accepts [donations](https://kde.org/fundraisers/yearend2022/).

## Packager section

You can find the package on [download.kde.org](https://download.kde.org/stable/tokodon/tokodon-23.02.0.tar.xz.mirrorlist) and it has been signed with my [GPG key](/gpg-02325448204e452a/).