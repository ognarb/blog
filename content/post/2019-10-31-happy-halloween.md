---
date: "2019-10-31T16:20:35Z"
title: Happy Halloween - Dark theme for more KDE website
---

Halloween was the perfect occasion for me to hack together a dark theme for
more KDE websites. So now a dark theme version is also available for
[kde.org](https://kde.org) and [planetkde](https://planet.kde.org).

It is using `prefers-color-scheme: dark` media query so it's only available
if you browser prefers the dark theme version.

![KDE.ORG dark theme](/assets/img/kde-org-dark.png)

![Planet kde dark theme](/assets/img/planetkde-dark.png)


