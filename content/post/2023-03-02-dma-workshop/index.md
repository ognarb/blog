---
title: Digital Market Act workshop in Brussels
date: "2023-03-02T12:00:00Z"
hideHeaderImage: true
aliases:
 - /2023/03/02/digial-market-act-workshop-in-brussels/
categories:
  - Free Software
  - KDE
  - NeoChat
  - EU
tags:
  - NeoChat
  - KDE
  - EU
image: visitor-pass.jpg
comments:
  host: floss.social
  username: carlschwan
  id: 109953735076413962
---

This Monday, I was in Brussels to attend a stakeholder workshop for the Digital Market Act (DMA) organized by the European Commission. For those who don't know that is the DMA, it's a new law that the European Parliament voted on recently and one of its goals of it is to force some interoperability between messaging services by allowing small players to able to communicate with users from the so-called Gatekeepers (e.g., WhatsApp).

I attended this meeting as a representative of KDE and NeoChat. NeoChat is a client for the Matrix protocol (a decentralized and end-to-end encrypted chat protocol).
I started developing it with Tobias Fella a few years ago during the covid lockdown.

I learned about this workshop thanks to NLNet, who funded previous work on NeoChat (end-to-end encryption). They put Tobias Fella and me in contact with Jean-Luc Dorel, the program officer for NGI0 for the European Commission. I would never have imagined sitting in a conference room in Brussels, thanks to my contribution to open-source projects,

![Visitor pass](visitor-pass.jpg)

Regarding the workshop itself, this was quite enlighting for me, who is just a minor player there and only work on NeoChat and many other KDE application in my free time as a volunteer. I expected a room full of lawyers and lobbyists, which was partially true. A considerable part of the room was people who were silent during the entire workshop, representing big companies and mostly taking notes.

Fortunately, a few good folks with more technical knowledge were also in the room. With, for example, people from Element/Matrix.org, XMPP, OpenMLS, Open Source Initiative (OSI), NlNet, European Digital Rights (EDRi), and consumer protection associations.

![Photo of the room](room.jpg)

The workshop consisted of three panels. The first was more general, and the latter two more technical.

In the first panel, the topic was the scope, the trade-offs, and the potential challenges of the Article 7 of the DMA. This panel was particularly well represented by a consumer protection organization, European Digital Rights, and a university professor, who were all in favor of the DMA and the interoperability part of it. Regarding the scope, one topic of discussion started by Simon Phipps was if gatekeepers like Meta should be forced to also interop with small self-hosted XMPP or Matrix instances or if this would only be about relatively big players. Unfortunately, I also learned that, while it was once part of the draft of the DMA, social networks are not required to interop. If Elon had bought Twitter earlier, this would have probably been part of the final text too. From this panel, I particularly appreciated the remarks of Jan Penfrat from the EDRi, who mentionned that this is not a technical or standardization problem and pointed out that some possible solutions like XMPP or Matrix already exist for a long time. There were also some questions left unanswered, like how to force gatekeepers to cooperate, as some people in the audience fear that they would make it needlessly difficult to interoperate.

After this panel, we had a short lunch, and this was the occasion for me to connect a bit with the Matrix, XMPP and NlNet folks in the room.

The second panel was more technical and was about end-to-end encryption. This panel had people from both sides of the debate. Paul Rösler, a cryptography researcher, tried to explain how end-to-end encryption works for the non-technical people in the audience, which I think was done quite well. Next, we had Eric Rescorla, the CTO of Mozilla, who also gave some additional insight into end-to-end encryption.

Cisco was also there, and they presented their relative success integrating other platforms with Webex (e.g. Teams and Slack) with for example, the possibility of creating Webex calls from Slack. This 'interoperability' between big players is definitively different from the direction of interoperability I want to see. But this is also a good example showing that when two big corporations want to integrate togethers, there are suddenly no technical difficulties anymore. They are also working on a new messaging standard (with reminds me a bit of [xkcd 927](https://xkcd.com/927/)) as part of the MIMI working group of the IETF and they already deployed that in production.

Afterward, it was the turn of Matrix, and Matthew Hodgson, the CEO/CTO at Element. Matthew showed a live demo of client-side bridging. This is their proposed solution to bridging end-to-end-encrypted messages across protocols without having to unencrypt the content inside a third-party server. This solution would be a temporary solution; ideally, services would converge to an open standard protocol like Matrix, XMPP, or something new. He pointed out that Apple was already doing that with iMessage and SMS. I found this particularly clever.

Last, Meta sent a lawyer to represent them. The lawyer was reading a piece of paper in a very blank tone. He spent the entirety of his allocated time, telling the commission that interoperability represents a very clear risk for their users who trust Meta to keep their data safe and end-to-end encrypted. He ignored Matthew's previous demo and told us that bridging would break their encryption. He also envisioned a clear opt-in policy to interoperability so that the users are aware that this will weaken their security and express a clear need for consent popups when interacting with users of other networks. It is quite ironic coming from Meta who in the context of the GDPR and data protection, was arguing against an opt-in policy and against consent. And as someone pointed out in the audience, while Whatsapp is end-to-end-encrypted, this isn't the case for Messengers and Instagram conversations, which are both also products of Meta. The lawyer quickly dismissed that and explained that he only represented Whatsapp here and couldn't answer this question for other Meta products. As you might have guessed, the audience wasn't convinced by these arguments. Still, something to note is that Meta had at least the courage to have something in front of the audience, unless other big gatekeepers like Microsoft, Apple and Google who were also in the room but didn't participate at all in the debate.

Finally, after another small coffee break, the last panel was about abuse prevention, identity management, and discovery. With Meta in the panel again, consent was again a hot subject of discussion. Some argued that each time someone from another server joins a room, each user should consent to this new server can read their messages. This sounds very impractical to me, but I guess this goal is to make interoperability impractical. It also reminds me very much of the GDPR popup, which privacy-invading services try to optimize using dark patterns so that the users click on the "Allow" button, just that in this case, this will try to convince the user to click on the "Don't connect with this user coming from this untrusted and scary third party server" button.

![Slides about efficient design for effective interoperability](slides.jpg)

There was also some discussion about whether this was the server's role in deciding if they allow connection from a third-party server or the user's role. The former would mean that big providers would only allow access to their service for other big providers and block access to small self-hosted instances, and the latter would give users a choice. Another topic was the identifier, which, as someone from the audience pointed out, phone numbers used by Whatsapp, Signal and Telegram are currently not perfect as they are not unique across services and might require some standardization.

In the end, the European Commission tried to summarize all the information they got today and sounded quite happy that so many technical folks were in the room and active in the conversation.

And finally, after the last panels, I went to a bar next to the conference building with a few people from XMPP, EDRi, NlNet and OpenMLS to get beers and Belgium fries.

![Me outside of the building](outside.jpg)
