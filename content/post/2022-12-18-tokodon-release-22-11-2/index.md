---
title: "Tokodon 22.11.2 release"
date: "2022-12-18T19:00:00Z"
hideHeaderImage: true
categories:
  - Free Software
  - KDE
  - Tokodon
tags:
  - Tokodon
  - KDE
image: tokodon.png
comments:
  host: floss.social
  username: carlschwan
  id: 109536563316859808
---

I'm happy to announce the release of Tokodon 22.11.2 (and 22.11.1 who I released earlier this month and forgot to properly announce). These releases contain mostly bug fixes but also some welcome interface improvements.

First this adds an account switcher (similar to the one Tobias Fella implemented in [NeoChat](https://apps.kde.org/neochat)). Very usefully when you need to manage multiple accounts and want to quickly switch between them.

![Tokodon](tokodon.png)

This also change the image preview from appearing in a separate full screen window to be contained inside the window. This follow the similar change from from James Graham in NeoChat.

![Preview full window mode](preview.png)

Joshua Goins improved the loading of media attachment and made it possible to now hide sensitive image by default using the blurhash effect. This is also using the already existing implementation of blurhash from Tobias in NeoChat and you might start to see a pattern in this release. ;)

![Blurhash post](blurhash.png)

Finally I added support for custom emojis in many places inside the UI. Perfect if you want to show you true verified checkmark in your profile :)

Aside from the nice new improvements, I improved the spacing in the app and while not perfect yet, I hope this makes Tokodon more enjoyable to use. Joshua Goins has also made various improvements to our internal networking code and this should offer better reliability and less prone to crash code. And I fixed an important crash on start-up that was affecting a lot of users

Finally I started adding unit tests in Tokodon and added the infrastructure to mock a Mastodon server. We now have reached 12% unit tests coverage and I hope this number will grow after each release.

<details>
<summary>And for those who prefer a full changelog, here it is:</summary>
<ul>
<li>Remember selected account</li>
<li>Update metadata</li>
<li>Fix rebasing issue</li>
<li>Attempt to fix Qt complaining about incomplete Post type</li>
<li>Add parents to replies made by Account::get and similar requests</li>
<li>More fixes</li>
<li>Move away from shared_ptr for Post</li>
<li>Fix double-free bug when viewing certain timeline pages</li>
<li>Add qtkeychain to .kde-ci.yml</li>
<li>Fix hide image icon missing on Android</li>
<li>View your own profile via account switcher</li>
<li>Add emoji support to page headings and profile bios</li>
<li>Fix translation extraction</li>
<li>Fix replying from the notification timeline</li>
<li>Fix notification list</li>
<li>Fix fetching the timeline twice</li>
<li>Release 22.11.2</li>
<li>Fix showing view-sensitive button too often</li>
<li>Don&#39;t have text autocomplete on login form</li>
<li>Add missing release info</li>
<li>Release 21.11.1</li>
<li>Remove usage of anchors in layout</li>
<li>Use blur hash for loading images and sensitive media</li>
<li>Improve hover effect on the card</li>
<li>Fix qt6 build</li>
<li>Fix dependency in the ci</li>
<li>Put accessible description at the bottom</li>
<li>Improve the look of cards</li>
<li>Use Kirigami.ActionToolBar</li>
<li>Allow download images</li>
<li>Full screen image like neochat</li>
<li>Add m_original_post_id for use in timeline fetch</li>
<li>Propertly reset pageStack when switching account</li>
<li>Polish NotificationPage</li>
<li>Improve layout of follow notification</li>
<li>Fix crash when switching account in the notification view</li>
<li>Fix translation catalog loading</li>
<li>Post: Fix memory leak</li>
<li>Fix off by one error in notification model</li>
<li>Posibly fix crash (second try)</li>
<li>Remove debug leftover</li>
<li>Posibly fix crash at startup</li>
<li>Improve account switcher</li>
<li>Make tap and text selection work together in PostDelegate</li>
<li>Fix wrong header url</li>
<li>Fix handling of empty displayName</li>
<li>Improve layout of PostDelegate</li>
<li>Add InteractionButton component for likes, boosts and replies</li>
<li>Add Qt 6 CI for FreeBSD and Android</li>
<li>Fix custom emoji in account lists</li>
<li>Port LoginPage to mobile form</li>
<li>Add runtime dependency for org.kde.sonnet</li>
<li>More cleanup and add autotests</li>
<li>Properly use getter and use displayNameHtml in more places</li>
<li>Implement custom emojis</li>
<li>Fix coverage badge</li>
<li>Add a refresh button for desktop devices</li>
<li>Reset message handler properly to prevent threads overwriting each other</li>
<li>Fix setInstanceUri early exit, preventing client id from being fetched</li>
<li>Add coverage badge</li>
<li>Fix reuse</li>
<li>Add a qDebug filter to remove confidential data</li>
<li>Add model tests</li>
<li>Add basic test</li>
<li>Split account in an abstract class without networking</li>
<li>Remot stray qDebug leaking token</li>
</ul>
</details>

## Packager section

You can find the package on [download.kde.org](https://download.kde.org/stable/tokodon/tokodon-22.11.2.tar.xz.mirrorlist) and it has been signed with my [GPG key](carlschwan.eu/gpg-02325448204e452a/).