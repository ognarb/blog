---
title: KDE PIM Sprint 2024 edition
date: "2024-06-16T10:10:35Z"
image: cake.jpg
hideHeaderImage: true
categories:
  - Free Software
  - KDE
tags:
  - KDE
comments:
  host: floss.social
  username: carlschwan
  id: 112628267952811335
---

This year again I participated to the KDE PIM Sprint in Toulouse. As always it
was really great to meet other KDE contributors and to work together for one
weekend. And as you might have seen on my
[Mastodon account](https://floss.social/@carlschwan), a lot of food was also
involved.

## Day 1 (Friday Afternoon)

We started our sprint on Thursday with a lunch at the legendary cake place,
which I missed last year due to my late arrival.

![Picture of some delicious cakes: a piece of cheesecake raspberry and basil, a piece of lemon tart with meringue and a piece of carrot cake)](cake.jpg)

We then went to the coworking space where we would spend the remaining of this
sprint and started working on defining tasks to work on and putting them on
real kanban board.

![A kanban board with tasks to discuss and to implement](kanban-start.jpg)

To get a good summary of the specific topics we discussed, I invite you to
consult the [blog of Kevin](https://ervin.ipsquad.net/blog/2024/06/16/report-from-kdepim-spring-sprint-2024/).

That day, aside from the high level discussion, I proceeded to port away the
IMAP authentification mechanism for Outlook accounts away from the KWallet API
to use the more generic QtKeychain API. I also removed a large dependency
from libkleo (the KDE library to interact with GPG).

## Day 2 (Saturday)

On the second day, we were greated by a wonderful breakfast (thanks Kevin).

![Picture of croissant, brioche and chocolatine](croissant.jpg)

I worked on moving EventViews (the library that renders
the calendar in KOrganizer) and IncidenceEditor (the library that provides
the event/todo editor in KOrganizer) to KOrganizer. This will allow to
reduce the number of libraries in PIM.

* [Remove KOrganizer plugins from kdepim-addons](https://invent.kde.org/pim/kdepim-addons/-/merge_requests/51)
  and [move them instead to KOrganizer](https://invent.kde.org/pim/korganizer/-/merge_requests/122)
* Do not use a complex plugin system for handling calendar invitation and instead
  provide the incidence editor as a binary and call it instead.
  ([MR 1](https://invent.kde.org/pim/akonadi-calendar/-/merge_requests/88),
  [MR 2](https://invent.kde.org/pim/kdepim-addons/-/merge_requests/52)
  and [MR 3](https://invent.kde.org/pim/incidenceeditor/-/merge_requests/59))

For lunch, we ended up eating at the excellent Mexican restaurant next to the
location of the previous sprint.

![Mexican food](mexican.jpg)

I also worked on [removing the "Add note" functionality in KMail](https://invent.kde.org/pim/kmail/-/merge_requests/132).
This feature allow to store notes to emails following [RFC5257](https://www.rfc-editor.org/rfc/rfc5257).
Unfortunatelty this RFC never left the EXPERIMENTAL state and so these notes
were only stored in Akonadi and not synchronized with any services.

This allow to remove the
[relevant widget from the pimcommon library](https://invent.kde.org/pim/pimcommon/-/merge_requests/41)
and [the Akonadi attribute](https://invent.kde.org/pim/akonadi/-/merge_requests/195).

I also started removing another type of notes: the KNotes app which
provided sticky notes. This application was not maintained anymore, didn't work
so well with Wayland. If you were using KNotes, to make sure you don't loose
your notes, I added support in Marknote to [import notes from KNotes](https://invent.kde.org/office/marknote/-/merge_requests/31).

![Marknote with the context menu to import notes](marknote.png)

Finally I worked on [removing visible Akonadi branding from some KDE PIM applications](https://invent.kde.org/pim/akonadi/-/merge_requests/193).
The branding was usually only visible when an issue occurred,
which didn't help with Akonadi reputation.

We ended up working quite late and ordering Pizzas. I personally got one with a lot
of cheese (but no photo this time).

## Day 3 (Sunday)

The final day, we didn't had any breakfast :( but instead a wonderful brunch.

![Picture of the brunch](brunch.jpg)

Aside from eating, I started writing a plugin system for the MimeTreeParser
which powers the email viewer in Merkuro and in Kleopatra. In the short term,
I want to be able to add Itinerary integration in Merkuro but in the longer
term the goal is to bring this email viewer to feature parity with the email
viewer from KMail and then replace the KMail email viewer with the one from
Merkuro. Aside from removing duplicate code, this will improve the security
since the individual email parts are isolated from each other and this will
makes it easier for the email view to follow KDE styling as this is just
normal QML instead of fake HTML components.

I also merged and rebased some WIP merge requests in Marknote in preparation of
a new release soon and reviewed merge requests from the others.

## Last but not least

If you want to know more or engage with us, please join the [KDE PIM](https://matrix.to/#/#kontact:kde.org) and the
[Merkuro](https://matrix.to/#/#merkuro:kde.org) matrix channels! Let’s chat
further.

Also, I’d like to thank again [Étincelle Coworking](https://www.etincelle-coworking.com/)
and [KDE e.V.](https://ev.kde.org/) to make this event possible. This wouldn’t be
possible without a venue and without at least partial support in the travel
expenses.

Finally, if you like such meetings to happen in the future so that we can push
forward your favorite software, please consider making a
[tax-deductible donation](https://www.kde.org/community/donations/index.php) to the
[KDE e.V. foundation](https://ev.kde.org/).