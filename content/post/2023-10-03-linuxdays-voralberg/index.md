---
title: Linux Days Voralberg
hideHeaderImage: true
date: "2023-10-03T14:00:00Z"
categories:
  - Free Software
  - KDE
  - Vacation
tags:
  - KDE
  - Vacation
image: dornbirn.jpg
comments:
  host: floss.social
  username: carlschwan
  id: 111172068031023854
---

Last weekend I went to the Linux Days in Voralberg (Austria) to host a booth
with Tobias and Kai. It was hosted at the Fachhochschule (a sort of university
for applied science) in Dornbirn and it was my first time attending this event.

![Me and Tobias in front of the LinuxDays poster at the entrance of the event](entrance.jpg)

Our booth was well visited and we had a lot of interesting discussions. As
always, we had various pieces of hardware on our booth: 2 laptops, a Steam Deck,
a Pinephone, a graphic tablet with Krita and two Konqi amigurumis.

![Our stand](stand.jpg)

Between booth duty, I still managed to watched one talk about open source
deployment in public institutions in Baden Wurtenberg (a region/state in German).
After the linux days, we all went to a restaurant and mass ordered Käsespätzle.
Käsespätzle is a traditional food from this region and is made of cheese, Spätzle
(noodles) and onions. It was excellent.

![Käsespätzle](food1.jpg)

On Sunday, Tobias and I went to Golm with a local we met the day before. We
took a gondola lift to reach a high-rope park in the mountains and then took an
Alpine Coaster to go back in the valley. It was a lot of fun.

![The view from the gondola](gondola.jpg)

![Picture of the high-rope pakr](park.jpg)

After our little adventure, we again went to eat in a traditional restaurant.

![Fish in a plate with noodles and pumpkin](food2.jpg)

Here a few more pictures of the trip:

![Dornbirn market place](dornbirn.jpg)

![Castel](castel.jpg)
