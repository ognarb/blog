---
date: "2020-04-19T00:00:00Z"
locale: fr
ref: kde-org-homepage
title: A new look for KDE.org
---

Commencé il y a longtemps et seulement fini maintainenant, j'ai le plaisir
d'annoncer la publication de la nouvelle page d'accueil de la communauté
KDE. J'éspère que vous allez l'appécier autant que je l'ai fait en la créant.

![KDE.org page d'accueil](/assets/img/kde-org-homepage.png)

Ceci n'est qu'un aperçu. Diriger vous vers [kde.org](https://kde.org) pour
voir la page complète.

Tout d'abord, j'ai enlever le carrousel. Certains etudes on montré qu'un
carrousel est un très mauvais moyen de transmettre une informaiton. Voir
"[should I use a carousel](http://shouldiuseacarousel.com/)".

À la place, j'ai créer divers sections au sujet de KDE. Plasma, les applications,
la hardware qui est livrée avec Plasma, et plus important encore la community KDE.
Ceci n'est qu'un simple aperçu, KDE a beaucoups plus de projects, mais j'espère
que cela donne une bonne introduction du travails que la communauté KDE fait depuis
plus de 20 ans. Si vous êtes intéressé, vous pouvez trouvé tous les produits que
nous créons sur la [page des produits KDE](https://kde.org/products).

La nouvelle page n'est pas seulement mon travails, mais un travail collaboratif.
Je veux remercier dans aucun ordre particulier Ilya Bizyaev, Niccolò Venerandi,
Paul Brown, manueljlin, David Cahalane, Blumen Herzenschein et d'autres
encore pour leurs aide dans la relécture, leur propositions et pour la creation
d'illustrations (merci encore manueljlin).

## Fun fact

Grâce à David Cahalane, j'ai apris que c'est la 8ème version de la page d'accueil de KDE.
Pour ceux interessé, j'ai creér une list d'archive de archive.org montrant
l'evolution de la page d'accueil.

 * [29 janvier 1998](https://web.archive.org/web/19980129141510/http://www.kde.org/index.html) C'est plus vieux que moi \o/
 * [3 marz 2000](https://web.archive.org/web/20000303113442/http://www.kde.org/index.html)
 * [2 marz 2001](https://web.archive.org/web/20010302060501/http://www.kde.org/)
 * [23 juin 2003](https://web.archive.org/web/20030623221955/http://www.kde.org/)
 * [18 juin 2009](https://web.archive.org/web/20090618162051/http://www.kde.org/)
 * [28 février 2013](https://web.archive.org/web/20130228134554/http://www.kde.org/)
 * [26 juin 2018](https://web.archive.org/web/20130228134554/http://www.kde.org/)
 * [20 avril 2020](https://kde.org) 

## Partciper

Il y a toujours plus de travail nécessaire pour diffuser KDE à travers le monde.
Créer des illustrations, perfectionner les sites web, écrire des annonces ne sont
que quelques-unes des tâches auxquelles vous pouvez contribuer. Rejoignez les groupes
KDE Web et KDE Promo and aidez-nous à faire connaître KDE au monde entier.

**KDE Web:**

* #kde-www
* [#freenode_#kde-www:matrix.org](https://webchat.kde.org/#/room/#freenode_#kde-www:matrix.org)
* [Telegram](https://t.me/KDEWeb)

**KDE Promo:**

* #kde-promo
* [#kde-promo:kde.org](https://webchat.kde.org/#/room/#kde-promo:kde.org)
* [Telegram](https://t.me/joinchat/AEyx-0O8HKlHV7Cg7ZoSyA)
