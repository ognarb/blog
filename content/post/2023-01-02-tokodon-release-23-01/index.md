---
title: "Tokodon 23.01.0 release"
aliases:
  - /2023/01/02/tokodon-23.01-release
date: "2023-01-02T12:00:00Z"
hideHeaderImage: true
categories:
  - Free Software
  - KDE
  - Tokodon
tags:
  - Tokodon
  - KDE
image: search.png
comments:
  host: floss.social
  username: carlschwan
  id: 109618705267814500
---

Happy new year! To get a good start in this new new year, I'm happy to announce that Tokodon 23.01.0 is out!
This is a new major release for Tokodon and while it's been only 2 weeks since the last major release, this
release is packed with new features and improvements.

Tokodon is a [Mastodon](https://joinmastodon.org/)/Pleroma/[Nextcloud Social](https://github.com/nextcloud/social) client built with [Kirigami](https://develop.kde.org/frameworks/kirigami/) that I started back in spring 2021. Tokodon has a great integration with KDE Plasma and Plasma Mobile, but it also work on other desktop environments and even Windows and macOS.

## Get it!

You can download Tokodon 23.01.0 from [Flathub](https://beta.flathub.org/apps/details/org.kde.tokodon) and from the F-Droid using the [KDE repo](https://community.kde.org/Android/FDroid). Currently we don't offer binaries for Windows and macOS but if you want to help with that, please get in touch.
build
## New timeline design

The first thing you will notice, when trying out this new version, is the new layout of the timelines.

I updated it to follow more closely the design of Mastodon web-UI and I also spent some time adjusting the spacing and typographie to be more consistent.

![Tokodon main view](main.png)

## Search

It is now possible to search for posts, accounts and tags directly from within Tokodon.

![Search field inside tokodon](search.png)

## Hashtag Handling

Tokodon now handles hashtags better. Previously, when clicking on a hashtag, Tokodon would redirect you to the website where you could see the posts that included the selected hashtag. Now you don't need to leave Tokodon anymore, and the posts are displayed inside the app like any other timeline.

## Conversation View

Tokodon now includes a basic conversation view. This shows the list of your last direct conversations. This makes it possible to see your conversation in a much nicer way than with the latest mentions view.

![Conversation in Tokodon](conversation.png)

I plan to improve this feature further in the future, as I will be adding support for conversation markers and maybe offer a real chat-like view for conversations (which I will shamelessly steal from NeoChat).

## Poll Support

It's now possible to view and interact with polls. This supports both multiple choice and single choice polls, but creating new polls is still not possible.

![Poll in Tokodon](poll.png)

## Account Editor

I also added an account editor. The merge request was actually 10 months old and I started it during a stream at the last FOSDEM. As you might imagine, it was not that fun to rebase. But it is finally possible to edit your account metadata and preferences directly from Tokodon.

![Account editor](account-editor.png)

I also submitted a [pull request in Mastodon itself](https://github.com/mastodon/mastodon/pull/22790) to expose more settings in the API.

## Real-Time Timeline Update

Tokodon was already handling some real-time events coming from the server, but until now this only included notifications. Now Tokodon will also handle new incoming messages and add them to the timeline. It will also delete messages as soon as the server asks for them to be deleted.

## Technical Details

Aside from the many new features, there was several large re-factorings of the code base to cleanup some code that younger-me wrote. The C++ code base now has a coverage of 35%, and I am pretty happy with this increase from 12% of the previous release.

Coming next, I want to focus more on the post editor. The back-end also needs to be cleaned up and several important features are still missing (choosing the language, posting polls and adding alternative textual descriptions to images). I already created a branch for that and you can follow the work on [this merge request](https://invent.kde.org/network/tokodon/-/merge_requests/98).


<details>
<summary>And for those who prefer a full changelog, here it is:</summary>
<ul>
<li>Update year</li>
<li>Update to 23.01</li>
<li>Cleanup notification model</li>
<li>Cleanup loading handling</li>
<li>Fix timezone handling</li>
<li>Remove dead code</li>
<li>Remove refreshing button now that refreshing happens automatically</li>
<li>implemented update notifications</li>
<li>Tweak appstream summary</li>
<li>timelinemodel fix crash if json array is empty</li>
<li>Fix unit tests</li>
<li>Fix loading timelines</li>
<li>Correctly handle streaming event about new and deleted posts</li>
<li>Remove debug output</li>
<li>Release new version 22.12.0</li>
<li>Cleanup and fix account model</li>
<li>Fix search on mobile</li>
<li>Reduce number of warning in logs</li>
<li>TootComposer.qml fix qmlscene issue &quot;missing &quot;:&quot;&quot;</li>
<li>Run clang format</li>
<li>Fix rendering of poll when no vote has been submitted</li>
<li>Handle pressing back button with image preview open</li>
<li>Remove dead code</li>
<li>AuthorizationPage.qml authorize token URL should wrap</li>
<li>Add more tests</li>
<li>Cleanup backend code for profile edition</li>
<li>Port to mobileform</li>
<li>Account editor (second try)</li>
<li>fix: No Preview for Hashtag links</li>
<li>Dont&#39;t show account switcher when no account is loaded</li>
<li>Fix clazy issues</li>
<li>Fix some compile warning</li>
<li>More testing</li>
<li>Add tests for threading</li>
<li>Improve thread UI</li>
<li>Fix opening thread</li>
<li>Clang format</li>
<li>Add more tests and fix canFetchMore behavior</li>
<li>Fix marking post as pinned</li>
<li>More refactoring</li>
<li>Remove fetching and fully replace by loading</li>
<li>Tag handling</li>
<li>Ship our own icon</li>
<li>Update icon name</li>
<li>Fix checkable state</li>
<li>Add unit test</li>
<li>Use breeze icons</li>
<li>Add conversation view</li>
<li>Remove dead code</li>
<li>Handle case where no spelled is installed</li>
<li>Fix crash in notification view when interacting with post</li>
<li>Propagate visibility in replies</li>
<li>Don&#39;t reset timeline when switching account</li>
<li>Fix accountModel returned by follow activity</li>
<li>Display blurhash when loading image</li>
<li>Don&#39;t show link preview on mobile</li>
<li>Fix replies count</li>
<li>Improve default debug output</li>
<li>Optimize tooltip in PostDelegate</li>
<li>Fix fetching the global timeline</li>
<li>Fix replying to someone add yourself as mention</li>
<li>Improve spacing of text field</li>
<li>Improve overall spacing</li>
<li>Add more unit tests</li>
<li>Implement searching for accounts and posts</li>
<li>Add a NetworkProxy page to settings</li>
<li>Port AuthorizationPage to MobileForm</li>
<li>Improve layout of already voted polls</li>
<li>Add more tests</li>
<li>Implement polls in the frontend</li>
<li>Add poll implementation in backend</li>
<li>Fix test</li>
<li>Reduce spacing between action and author</li>
<li>Update post layout</li>
<li>Remove duplicate headers between cpp/h files</li>
<li>Workaround kirigami regression</li>
<li>Don&#39;t show drawer handle when showing full screen image</li>
<li>Fix crash when loading own account</li>
<li>Fix appdata version</li>
</ul>
</details>


## Get Involved

If you are interested in helping, don't hesitate to reach out in the Tokodon matrix channel (#tokodon:kde.org) and I would be happy to guide you.

I'm also regularly posting about my progress on Tokodon (and other KDE apps) on my [Mastodon account](https://floss.social/@carlschwan), so don't hesitate to follow me ;)

And in case, you missed it, as a member of the fundraising working group, I need to remind you that KDE is currently running an [end of the year campaign](https://kde.org/fundraisers/yearend2022/). Don't hesitate to donate!

## Packager section

You can find the package on [download.kde.org](https://download.kde.org/stable/tokodon/tokodon-23.01.0.tar.xz.mirrorlist) and it has been signed with my [GPG key](/gpg-02325448204e452a/).
