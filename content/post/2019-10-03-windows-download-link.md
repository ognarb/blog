---
author: Carl Schwan
date: "2019-10-03T11:35:35Z"
description: Windows download link in kde.org/applications
locale: en
title: Windows download link in kde.org/applications
---

Some quick news about a new feature of kde.org/applications! With the amazing work
done by Hannah von Reth and Christoph Cullmann, we now have more applications
in the Windows Store.

* [KDE applications on Windows](https://cullmann.io/posts/kde-applications-on-windows/)

I updated [kde.org/applications](https://kde.org/applications) to display this
important information. Now if you are browsing for example the
[Kate application page](https://kde.org/applications/utilities/org.kde.kate) using Windows,
you should see a button "Install on Windows" instead of "Install of Linux".

![Install on Windows button](/assets/img/kde-application-windows.png)

Since the generation of this website use the AppStream standard and this standard
allow to add some [custom fields](https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-custom),
I added a vendor prefixed field to it: `KDE::windows_store`. Et voilà, the metadata
is available to the php code.

The next task is to add a list at the bottom of the supported platform, in case the operating
system detection doesn't work. This will let the visitor know about all the supported
platforms.

If someone is interested in doing this task, please contact me on irc/matrix #kde-www
and username 'Carl Schwan/CarlSchwan[m]'. The repository is available in the new
KDE Gitlab instance: [invent.kde.org/websites/kde-org-applications/](https://invent.kde.org/websites/kde-org-applications/).

<style>
.post-content img { text-align: center; }
</style>

