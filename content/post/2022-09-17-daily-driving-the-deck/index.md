---
title: Daily driving the steam deck
date: "2022-09-17T10:00:00Z"
image: /2022/09/17/daily-driving-the-steam-deck/steamdeck-workstation.jpg
hideHeaderImage: true
---

Tuesday night, I managed to break the screen of my laptop. This is particular annoying when you don't have any external screen at home and need to work. Fortunately the scren wasn't completely broken, and I managed to survice Wednesday, with half of the screen working.

From half of the screen working on Wednesday, the situation got worse on Tursday and I was forced to find another solution. My only other Linux powered devices at home were a PinePhone and my SteamDeck. Performance wise, the choice was easy and I choose to try to use my SteamDeck. And so my workstation on Tursday and Friday ended up like this:

![Steam deck workstation](steamdeck-workstation.jpg)

I connected the dev environment of my laptop with SSH and it kinda worked. I did some commit. I even managed to do call and share my screen for a demo/presentation at work. But still the experience on the small screen wasn't that great.

Fortunately, I ordered a new laptop (Thinkpad E14 gen 4) and it arrived on Friday afternoon. The new laptop has a working screen but no mainline wifi drivers. So took me a bit more time than expected to build and sign the out-of-tree drivers with secure boot enabled.

I love hardware.
