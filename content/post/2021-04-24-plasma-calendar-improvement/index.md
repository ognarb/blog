---
title: Plasma Calendar Redesign
date: "2021-04-24T11:35:35Z"
image: calendar1.png
hideHeaderImage: true
categories:
  - Free Software
  - KDE
  - Plasma
tags:
  - Plasma
  - KDE
comments:
  host: linuxrocks.online
  username: carl
  id: 106120290801170192
---

Over the last few weeks, I redesigned the default Plasma Calendar
bringing it more in line with the design we want for Breeze Evolution.
The new design removes the lines between the days, uses the default
Plasma highlighting element and uses the Plasma Header component
to provide a consistent look for the headers.

![The new calendar design](calendar1.png)

There were also improvements in term of usability. Most notably, it
is now easier to switch between the month, year and decade overview.
Before, you needed to discover that the month name was clickable but
now just clicking on the day, month or year button works.
Additionally, on a touch screen, you can also swipe the calendar left
or right.

<figure style="max-width: 600px; margin-left: auto; margin-right: auto;" class="embed-responsive embed-responsive-16by9">
  <video src="https://carlschwan.eu/2021/04/24/plasma-calendar-redesign/calendar.mp4" loop autoplay muted></video>
  <figcaption style="margin-top: 3rem;">Swiping with the finger or the mouse works now</figcaption>
</figure>

These improvements will be available with Plasma 5.22, but some will
land a bit earlier with KDE Framework 5.82.

## What is coming next?

The next step in improving the calendar support in Plasma is improving
the event representation in the month view. I already have
a merge request ready for that, replacing the triangle with colored
dots. Don't worry if you have a very busy shedules, it won't display
more than five at the same time.

![Calendar with dots for events, much more pleasing for my eyes :D](calendar2.png)

You can take a look at it in [frameworks/plasma-framework!243](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/243).

Before the Plasma 5.22 feature freeze, I want to land a few more features.

* Add a button to add events (by opening Korganizer in case it is
installed)

* Enable the event display by default when Korganizer and kdepim-addons
are installed. This would improve the visibility of the features and
make the calendar applet much more useful.

## Update

Since some people asked me about how it looks with a dark theme.

![Dark theme](darl.png)

Also the MR for opening Korganizer [is ready](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/814/diffs)! 
