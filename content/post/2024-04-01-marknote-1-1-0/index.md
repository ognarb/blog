---
title: Marknote 1.1.0
date: "2024-04-01T15:05:00Z"
hideHeaderImage: true
comments:
  host: floss.social
  username: kde
  id: 112196947085009576
categories:
  - Free Software
  - KDE
tags:
  - KDE
image: commandbar.png
---

Marknote 1.1.0 is out! Marknote is the new WYSIWYG note-taking application from
KDE. Despite the latest release being just a few days ago, we have been hard at
work and added a few new features and, more importantly, fixed some bugs.

Marknote now boasts broader Markdown support, and can now display images and
task lists in the editor. And once you are done editing your notes, you can
export them to various formats, including PDF, HTML and ODT.

![Export to PDF, HTML and ODT](export.png)

Marknote's interface now seamlessly integrates the colors assigned to your
notebooks, enhancing its visual coherence and making it easier to distinguish
one notebook from another. Additionally, your notebooks remember the last
opened note, automatically reopening it upon selection.

![Accent color in list delegate](colors.png)

We've also introduced a convenient command bar similar to the one in Merkuro.
This provides quick access to essential actions within Marknote. Currently it
only creates a new notebook and note, but we plan to make more actions
available in the future. Finally we have reworked all the dialogs in Markdown
to use the newly introduced [FormCardDialog from KirigamiAddons](/2024/04/01/kirigami-addons-1.1.0/).

![Command bar](commandbar.png)

We have created a small [feature roadmap](https://invent.kde.org/office/marknote/-/issues/27)
with features we would like to add in the future. Contributions are welcome!

## Packager section

You can find the package on
[download.kde.org](https://download.kde.org/stable/marknote/marknote-1.1.1.tar.xz.mirrorlist)
and it has been signed with my [GPG key](/gpg-02325448204e452a/).

Note that this release introduce a new recommanded dependencies: md4c and require
the latest Kirigami Addons release (published a few hours ago).
