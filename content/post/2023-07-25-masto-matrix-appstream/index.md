---
title: Mastodon and Matrix link in your AppStream file
date: "2023-07-25T21:00:00Z"
categories:
  - Free Software
  - KDE
tags:
  - KDE
---

Quick reminder, that you can include a Mastodon or Matrix link to your AppStream file and that the link will then be displayed in your application page at [apps.kde.org](https://apps.kde.org).

```xml
<custom>
  <value key="KDE::matrix">#merkuro:kde.org</value>
  <value key="KDE::mastodon">https://kde.social/@merkuro</value>
</custom>
```

![Details on apps.kde.org showing the link to the mastodon account and matrix channel](app-details.png)