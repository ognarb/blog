---
title: Frameless view with QtWidgets
date: "2023-08-29T17:00:00Z"
hideHeaderImage: true
categories:
  - Free Software
  - KDE
  - Plasma
tags:
  - Plasma
  - KDE
image: dolphin.png
comments:
  host: floss.social
  username: carlschwan
  id: 110974124812274823
---

One design characteristic of our QtWidgets is that they contain a lot of
frames and frames inside other frames. This worked well with Oxygen style and
its skeuomorphism shadow, less so with Breeze.

I first thought this was inheriten with QtWidgets and couldn't be fixed without
much effort. But fortunately, after looking a bit into Qt source codes and in
particular in the internals of QDockAreaLayout, I discovered that the engine to
draw and style the built-in components of QtWidgets: QStyle has a
`QStyle::PE_IndicatorDockWidgetResizeHandle` primitive which allows drawing
separators between detachable docks and similarly there is `QStyle::CE_Splitter`
to paint the separator between elements inside a QSplitter. This is huge
because this means instead of drawing frames, we can render separator and then
get rid of most of our frames in our apps.

This is how Qt Linguist tool looks like with this change.

![Linguist with instead of using frames for each dock, use separators](linguistic.png)

Unfortunately, there are still some places where we do want to draw frames, so
we can't remove them all. I added a heuristic that tries to determine when to
draw one based on the spacing and margins of the parent layout. A heuristic is
never perfect, but an app can additionally force the style to display a frame
or vice versa force the style not to display it.

For more complex apps (read with more custom components), this change require a
bit of tweaking but with a handful of one-liner in Kate, I got this modern look.

![Kate without frames](kate.png)

This is not perfect yet and this will require a bit more tweaking around the
tab bar, but is already a big departure from the current style.

Similar, this is how Dolphin and Ark, look with these changes:

![Dolphin with a frameless split view](dolphin.png)

![Ark with 3 panes, one on the top left with a the archive content, one on the right with the name of the archive and one on the bottom left with the archive comment](ark.png)

If you like this change, don't hesitate to put a 👍 on the
[pending MR](https://invent.kde.org/plasma/breeze/-/merge_requests/342), and if
you are a developer, please test your app with this change and look at how I
adapted a few apps already. I tried the change with some big apps like Krita
and Kdevelop and it looks good, but the more testing, the better it is.
