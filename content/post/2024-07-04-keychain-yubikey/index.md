---
title: "Keychain Development Update: Yubikey Support"
date: "2024-07-04T00:00:00Z"
categories:
  - Free Software
  - KDE
tags:
  - KDE
  - Keychain
hideHeaderImage: true
image: edit-group.png
---

Following my [latest post about Keychain](/2024/07/01/initial-work-on-keychain/),
here is a new development update. Yubikey and Key Files are now supported,
which allows you to requires a YubiKey to open a password database but also to
save it.

<figure style="max-width: 100%; margin-left: auto; margin-right: auto;" class="embed-responsive embed-responsive-16by9">
  <video src="yubikey-unlock.mp4" loop autoplay muted></video>
</figure>

Saving and editing groups also now works.

![Group editing dialog](edit-group.png)

And I now started working on the database creation process. The UI is ready but
I still need to bind it to the backend.

Thanks to everyone who send me encouragement messages and also to Laurent who
did a lot of
[cleanups](https://invent.kde.org/carlschwan/keychain/-/commits/master?ref_type=heads)
in the codebase.

See you in the next development update.