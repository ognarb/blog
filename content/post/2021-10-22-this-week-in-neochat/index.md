---
title: This week in NeoChat
hideHeaderImage: true
date: "2021-10-22T10:00:35Z"
comments:
  host: fosstodon.org
  id: 107143945407882524
  username: neochat
image: sprint.jpg
---

Last Saturday we had an improvised NeoChat mini development sprint in a small hotel room in Berlin in the occasion of the 25th anniversary of KDE. In a good KDE tradition, Carl spent this time on improving NeoChat settings. He ported both the NeoChat general settings and the specific room settings to the new `Kirigami.CategorizedSetting` component.

Tobias fixed a lot of papercuts and now the power level should be fetched correctly, we show the number of joined users instead of joined+invited users in the room information pane, the user search is now case insensitive.

Nicolas focused on fixing our Android build by making the spellchecking feature compile on Android.

![General settings](general.png)

![Roon settings](roomSetting.png)

![Picture of the hotel room where we did the improved spring](sprint.jpg)

Aside from the mini-sprint, we also made a few more improvements during the week. Tobias fixed the flicking of the timeline on mobile and Carl made it possible for the user to resize the room information drawer.