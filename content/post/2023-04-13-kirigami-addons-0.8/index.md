---
title: Kirigami Addons 0.8.0
date: "2023-04-13T18:00:00Z"
hideHeaderImage: true
categories:
  - Free Software
  - KDE
  - Arianna
tags:
  - Arianna
  - KDE
image: spinbox-desktop.png
comments:
  host: floss.social
  username: carlschwan
  id: 110193329091127813
---

My second release of the day: Kirigami Addons 0.8.0. This release contains a
few new components.

## AbstractMaximizeComponent

This is part of the `org.kde.kirigamiaddons.labs.components` module and is a
popup that covers the entire window to show some items. This is already used in
NeoChat and Tokodon to magnify image and videos.

Thanks James Graham for developing the initial version and upstreaming it to
Kirigami-addons.

![Maximized image in NeoChat](maximized.png)

## Convergent SpinBox

Another new component is the convergent `SpinBox` from the
`org.kde.kirigamiaddons.labs.mobileform` module.

This is just a normal spinbox on desktop.

![Spinbox on desktop with small touch targets](spinbox-desktop.png)

But on Plasma Mobile/Android the touch target becomes bigger.

![Spinbox on desktop with larger touch targets](spinbox-mobile.png)

## Others

Aside from this two components this release contains some small bugfixes and
other minor API improvememts.

## Get Involved

If you are interested in helping, don't hesitate to reach out in the Plasma Mobile
matrix channel ([#plasmamobile:kde.org](https://matrix.to/#/#plasmamobile:kde.org)) and I
will be happy to guide you.

And in case, you missed it, as a member of KDE's fundraising working group, I
need to remind you that KDE e.V., the non-profit behind the KDE community
accepts [donations](https://kde.org/fundraisers/yearend2022/).

## Packager section

You can find the package on
[download.kde.org](https://download.kde.org/stable/kirigami-addons/kirigami-addons-0.8.0.tar.xz.mirrorlist)
and it has been signed with my [GPG key](/gpg-02325448204e452a/).