---
title: 'Hash-o-Matic 1.0.1'
date: "2024-06-25T10:10:35Z"
categories:
  - Free Software
  - KDE
tags:
  - KDE
---

Hash-o-Matic 1.0.1 is out! Hash-o-Matic is a tool to compare and generate
checksum for your files to verify the authenticity of them. It also verify
files via their use PGP signatures.

This new release of Hash-o-Matic provides updated translations and some small
visual changes. In the background, the application was ported to the new QML
type registration, we now support building Hash-o-Matic on Haiku and we now
require released version of KDE Frameworks instead of pre-released version.

## Packager Section

You can find the package on
[download.kde.org](https://download.kde.org/stable/hash-o-matic/hash-o-matic-1.0.1.tar.xz.mirrorlist)
and it has been signed with my [GPG key](/gpg-02325448204e452a/).