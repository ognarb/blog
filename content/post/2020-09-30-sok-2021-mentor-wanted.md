---
date: "2020-09-30T11:40:00Z"
locale: en
title: 'SoK 2021: Mentor Wanted!'
image: https://kde.org/content/people/GSoC.jpg
---

The Season of KDE is a 3 weeks long program that provides an opportunity for
people to do mentored projects for KDE.

We are still looking for more mentors for SoK 2021. So please consider mentoring
for this year season and adding ideas related to the project you are working on
in the [Wiki page](https://community.kde.org/SoK/Ideas/2021). And joining the #kde-soc
channel.

Possible ideas includes but not limited to:

* Updating your application user documentation
* Write technical documentation for a framework
* Port your application to non-deprecated Qt and KF5 APIs
* Modernize some part of the code of an application
* Implement a new feature
* And a lot more, SoK is not limited to development :)
