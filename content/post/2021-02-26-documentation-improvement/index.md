---
title: Documentation Improvements in KDE
date: "2021-02-26T17:00:00Z"
hideHeaderImage: true
categories:
  - Free Software
  - KDE
  - Blog
  - Documentation
tags:
  - Documentation
  - KDE
image: api.png
comments:
  host: linuxrocks.online
  username: carl
  id: 105798477266183469
---

There was many changes over the last few months in KDE developer
documentation tooling. The hope is to make KDE development easier
to both newcomers but also long-time KDE contributors to use
KDE technologies to build cool stuff.

## Api documentation

The tooling for our generated documentation tooling improved. First
of all, KApiDox got a new theme with a cleaner appearance and a
better dark theme. But the improvement goes beyond just theming.

![API.kde.org new look](api.png)

[Doxyqml](https://invent.kde.org/sdk/doxyqml), our documentation
bridge between QML and doxygen, got various improvements, thanks
to Olaf Mandel and Lasse Lopperi. Now QML enums are supported and
the lexer/parser got various bug fixes.

Speaking of QML documentation, the Kirigami API documentation was
improved and now uses more correctly `@inherit` tags and
`@property` tags. There is still room for improvements, but the
current state is already a lot better. Most Components are now
showing all their properties correctly and the type of the
property is correct. ([kirigami!239](https://invent.kde.org/frameworks/kirigami/-/commit/6ce80224a9fe46216cf47d29458e16f3519ec693))

Another improvement is that the generated Kirigami documentation
now shows more accurate names: e.g. `Kirigami.Page` instead of
`org::kde::kirigami::Page`. This makes it easier to read and
navigate the documentation.

There was also a bit of background work inside KApiDox, Jannet
added support for QDoc, allowing to use QDoc as an alternative
to Doxygen. This might be a better solution for generating
documentation for projects with a lot of QML.

## High level documentation

Since [develop.kde.org](https://develop.kde.org) was announced
back in September 2020 just before Akademy, it received a steady
stream of updates. In terms of visuals, Jannet replaced the left
sidebar with a better-looking one.

A Doxygen integration was also added in the Hugo based website,
allowing to link to the API documentation in an easy way (e.g.
`[Kirigami.Page](docs:kirigami2;Page)`).

![Link to API documentation](doxapi.png)

The Plasma documentation was massively improved. Zren moved his
excellent tutorial about creating Plasma Widgets to
[/docs/plasma/widget](https://develop.kde.org/docs/plasma/widget/).
I also moved the [Plasma Desktop Scripting tutorial](https://develop.kde.org/docs/plasma/scripting/)
and the [Plasma Theme Tutorial](https://develop.kde.org/docs/plasma/theme/)
from techbase to develop. This was a good occasion to update them to a
more recent version of Plasma since some parts were from the early
days of Plasma 5 and a few bits from KDE4 remained.

Concerning the Framework documentation, most of the tutorials from
the Framework book, that was written a few years ago to develop.
Same as the Plasma documentation, I used this opportunity to update
the documentation and remove any mentions of deprecated APIS. I
also did the same for most of the old tutorials from [techbase](https://techbase.kde.org).

![List of features oriented tutorials](features.png)

Tobias Fella imported the old [DBus tutorial](https://develop.kde.org/docs/d-bus/)
from techbase and David Redondo wrote a tutorial about how to
[create sensor faces](https://develop.kde.org/docs/sensor-faces/)
for the new Plasma Monitor.

![Custom sensor faces](https://develop.kde.org/docs/sensor-faces/images/config.png)

Finally, Clau Cambra wrote a tutorial for getting started in
[Kirigami](https://develop.kde.org/docs/kirigami/). The tutorial
will guide you into creating a countdown counter using Kirigami
and QML. This work is part of its Season of KDE project.

If you enjoy my work, you can sponsor me on [Liberapay](https://liberapay.com/Carl).
