---
title: Improvements to QTextDocument
date: "2024-04-14T20:00:35Z"
image: table-new.png
hideHeaderImage: true
comments:
  host: floss.social
  username: carlschwan
  id: 112271753404024004
---

[Marknote](https://apps.kde.org/marknote/) uses QTextDocument for its WYSIWYG
text editor. This is surpringly quite powerful and thanks to some code borrowed
from KMail rich text editor, it wasn't hard to implement huge part of the
markdown specification.

But while QTextDocument is great, I hit quickly some limits. This is why I
started fixing some of them and I already have some patches up for review in Qt.

## Default table style

By default the style of the tables looks straight from the 90s, I submitted a
patch to use something a bit nicer: https://codereview.qt-project.org/c/qt/qtbase/+/554132
(merged!)

![Old style](table-old.png)

![New style](table-new.png)

This is easy to change in the app itself and this is already the case in
Marknote and NeoChat table rendering, but by default I believe Qt should
provides a nice style so that apps don't need to figure out how to overwrite
the default style.

While working on this, I also noticed that the border-collapse property was not
supported by the layout engine in QtQuick and only in QtWidget. This resulted
in the border of the tables to be twice as thick as they should be. This was
fortunately fixed by a simple patch:
https://codereview.qt-project.org/c/qt/qtdeclarative/+/554151 (merged!)

## Responsive images

A width and an height can be assigned to an image in a QTextDocument by using
the respectives html attributes `<img height="500" width="500" \>`.This works
correctly when the window size is known and static but less so then the window
can be resized as will be the user. A common technique in web design is to add
the following styles to the website:

```css
img {
    max-width: 100%;
}
```

And while QTextDocument support some CSS properties, `max-width` wasn't
implemented. Adding the support for more properties in the HTML and CSS
parser is quite simple as it was just forwarding the value of the max-width
attribute to the QTextImageHandler, and a bit complicated was the handling of
the `%` unit as previously only `px` and `em` was supported by Qt.

This resulted in two more patches, one for [QtTextDocument and QtWidget](https://codereview.qt-project.org/c/qt/qtbase/+/554130)
and one for [QtQuick](https://codereview.qt-project.org/c/qt/qtdeclarative/+/554131).

<div class="embed-responsive embed-responsive-16by9">
  <video class="embed-responsive-item" src="https://carlschwan.eu/2024/04/14/improvements-to-qtextdocument/img-fluid.mp4" controls allowfullscreen></video>
</div>

## Future

Hopefully these patches will be reviewed and merged soon. Afterward I want
to add support for a few more CSS properties like: `border-radius` for images
and some other elements, as well the `border-{width, style, color}` properties
for paragraphs to better support `<blockquote />` (important when displaying
emails).