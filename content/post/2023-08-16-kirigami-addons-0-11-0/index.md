---
title: Kirigami Addons 0.11.0
date: "2023-08-16T06:00:00Z"
hideHeaderImage: true
categories:
  - Free Software
  - KDE
  - KirigamiAddons
tags:
  - KirigamiAddons
  - KDE
image: about1.png
---

Kirigami Addons 0.11.0 is out! This release brings a bunch of new components.
Since I forgot to write an announcement for the 0.10 release, I will mention
some of the new features of 0.10 too.

## Banner (0.10.0)

This helpful component is similar to `Kirigami.InlineMessage` and can be used
as the footer or header of a page.

![Banner component at the top of page](banner.webp)

## Delegates (0.10.0)

Kirigami Addons 0.10.0 bring two new list and grid delegates:

* `RoundedItemDelegate:` This delegate provides a nice rounded background for items
inside a list or grid. You can see it in action in Arianna where it is used both
in the grid and the sidebar.

![Grid rounded delegates and list rounded delegate](delegates.png)

* `IndicatorItemDelegate`: This component is the perfect list delegate for an inbox
where elements can be marked as read or unread (e.g in an email client).

![Merkuro Mail folder view where some emails are marked as read and others are still unread](mail.png)

## Avatar (0.10.0)

We moved `Kirigami.Avatar` from Kirigami to Kirigami Addons. We tweaked the API a
bit at the same time, and `Avatar` is no longer based on the `AbstractButton` component
but is just an `Item` which can be used for decorative purposes.

In 0.11.0, we also introduced AvatarButton that can be used if you need an
interactable element.

We also updated the look of the placeholder, when no avatar images is found, to
be a bit less visually heavy and use a pale color. Fun fact, this is a design I
helped introduce in Nextcloud a year ago.

![New avatar look](avatar.png)

## MobileForm.FormHeader (0.10.0)

`FormCardHeader` is now deprecated, and we are replacing it with `FormHeader`.
The difference is that `FormHeader` is placed outside of the cards.

## Kirigami Settings (0.11.0)

We moved `Kirigami.CategorizedSettings` and `Kirigami.SettingAction` from
Kirigami to Kirigami Addons. We used this opportunity to do a visual refresh of
the component.

![NeoChat settings using both the new Settings and FormHeader component](kirigami-settings.png)

Currently, the search feature only searches inside the title of the categories, but
in the future, we would like it to also search inside the content of the pages.

## Floating buttons (0.11.0)

In KF6, for Kirigami we are removing the automatic floating buttons. As a replacement,
we are adding two new components: `FloatingButton` and `DoubleFloatingButton`. We
extracted these components from [Powerplant](https://apps.kde.org/powerplant),
[Audiotube](https://apps.kde.org/audiotube) and [Marknote](https://apps.kde.org/marknote).

This is how this looks in Powerplant.

![Floating button at the bottom right](powerplant.png)

## MobileForm is now FormCard (0.11.0)

We decided to rename MobileForm to FormCard since this new layout is also used
on other form factors. As part of the renaming we used the opportunity to do
some small but welcome change to the api. A lot of boilerplate for the layout
is not needed anymore.

```diff
diff --git a/src/qml/RoomSettings/Permissions.qml b/src/qml/RoomSettings/Permissions.qml
index 07b8a942..674ee4a5 100644
--- a/src/qml/RoomSettings/Permissions.qml
+++ b/src/qml/RoomSettings/Permissions.qml
-import org.kde.kirigamiaddons.labs.mobileform 0.1 as MobileForm
+import org.kde.kirigamiaddons.formcard 1.0 as FormCard

-Kirigami.ScrollablePage {
+FormCard.FormCardPage {
-    id: root
-
-    title: i18nc("@title:window", "Notifications")
-    topPadding: 0
-    leftPadding: 0
-    rightPadding: 0
-    ColumnLayout {
-        spacing: 0
-        MobileForm.FormCard {
-            contentItem: ColumnLayout {
-                spacing: 0
-
-                MobileForm.FormCardHeader {
-                    Layout.fillWidth: true
-                    title: i18n("Room notifications setting")
-                }
-
-                MobileForm.FormRadioDelegate {
-                    text: i18n("Follow global setting")
-                    onToggled: { ... }
-                }
-            }
-        }
-    }
+
+    FormCard.FormHeader {
+        title: i18n("Room notifications setting")
+    }
+
+    FormCard.FormCard {
+        FormCard.FormRadioDelegate {
+            text: i18n("Follow global setting")
+            onToggled: { ... }
+        }
+    }
```

The qml import `org.kde.kirigamiaddons.labs.mobileform` is still here and will
remain for the foreseeable future. Still, please update to the new import name to
get all the latest improvements.

## New About Pages

The look of the about page and the about kde page was also updated to use the
new Avatar and FormCard components.

![](about1.png)

![](about2.png)

![](about3.png)

## Bug fixes and minor improvements

- We fixed various issues with the translations not loading for some components
  and added some ci checks to ensure that this doesn't happen in the future.
- In AlbumMaximizeComponent, we are now using icon name compatible with more
  xdg-icon-themes
- The AboutPage now displays more information which were previously not displayed
  due to some broken checks.

## Packager section

You can find the package on
[download.kde.org](https://download.kde.org/stable/kirigami-addons/kirigami-addons-0.11.0.tar.xz.mirrorlist)
and it has been signed with my [GPG key](/gpg-02325448204e452a/).