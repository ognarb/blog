---
title: NeoChat 1.2
date: "2021-06-02T10:00:35Z"
image: bubbles.png
hideHeaderImage: true
effects:
  - name: Fireworks
    url: firework.png
  - name: Snow
    url: snow.png
  - name: Confetti
    url: tada.png
---

The NeoChat is happy to announce the 3 major version of NeoChat.
This release is the product of more than 3 months worth of work.

## Bubbles everywhere 🗨️

The first thing you will see then opening NeoChat 1.2 is that we
are now using message bubbles.

![Bubbles 🗨️ everywhere 🗨️](bubbles.png)

We also replaced the not so great green line used as read marker
with a better looking separator.

## Advanced Chatbar

The text input component was completely redesigned.

![Editing a message](editing.png)

![Replying to someone](replying.png)

![Adding an attachment](attachment.png)

It's also now possible to get autocompletion of commands:

![Use commands the easy way!](commands.png)

Speaking of commands, it's now possible to send customized reactions
when replying to a message with `/react I love NeoChat`.

![Custom reactions](custom_reaction.png)

There was also improvements in the keyboard shortcuts. It's now possible
to use <kbd>⬆️</kbd> to jump in edit mode for the last message.

![Quick Edit](quickedit.gif)

Another way to edit the last message is to use the `s/text/replacement`
syntax inspired by `sed`. It's not enabled by default but you can find
it in the settings.

## Matrix URI support

NeoChat now support matrix uris and when opening `matrix:` link
in your browser you will then be proposed to open it with NeoChat.
As a side effect, supporting Matrix URIs helped us unify how we handle
opening and joining rooms accross our codebase.

## Inline reply

We added inline replies support in our notification. So it's now possible
to reply to messages directly from the notification.

![Inline reply](inlinereply.png)

## Fancy effects

Miss the fancy fireworks or snow effects from Element? We too. So Alexey Andreyev
added them in NeoChat.

{{< screenshots name="effects" >}}

## Mobile Improvements

On mobile the context menu are implemented as bottom drawer.

![User Information Dialog on Mobile](menu-mobile.png)

## Account switcher

NeoChat supports multiple accounts since day one but it was not really practical
to switch between accounts. We now implemented an account switcher, available
at the bottom of the room list.

![Account Switcher](accountswitcher.png)

## Other

Aside from the many new big features this release brings, NeoChat 1.2 contains
a lot of bugfixes and small usability improvements. We closed aproximately
100 issues (bugs and feature request) and this doesn't count the huge number
of bugs fixed without an corresponding issues. Some hightlight are:

* Scrolling on mobile now is smoother. It still not perfect but we are
working on making it better.
* Joining a room now makes it appears in the room list without having to
restart NeoChat and the room will be automatically opened
* Show the location of hyperlinked text on hover.
* Add an indicator for lack of internet connectivity
* NeoChat doesn't hang anymore when loading a room
* Disable the chatbox if we're not allowed to send messages 

{{< swiper >}}
