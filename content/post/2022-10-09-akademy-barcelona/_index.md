---
title: Akademy in Barcelona
draft: true
---

Last week I went to Barcelona to attempt Akademy, the yearly KDE conference,
and this time it was in person. This is my forth Akademy and my second one
in person.

## The travel

Due to environmental concerns, I decided with a few others to take the train
instead of a flight. This wasn't without trouble unfortunately. First two from
our train-travelers group ended up canceling their travel due to covid. Good
reminders that even if nobody talks about covid anymore, the virus is still here
and ruins people travel plans.

Fortunately some of us could still make it and Harald, Tobias and I ended up meeting in Strasbourg on Wednesday late afternoon to visit a bit the city and explore a bit the bars. It was also the occasion to meet an old flatmate of mine, eat some flamenkucken and go to the Delirium for some good Belgium
beers.

On Thursday morning, Felix joined us and we boarded our train. It unfortunately
wasn't a train ride without any issues. The SNCF (the french train company) decided to go on a strike and we ended up being stuck in Valence for a few hours, we had to book some new tickets and we arrived in Barcelona later as planned but in the same train as Joseph who was traveling from Paris. In a good Spanish tradition, we ended up eating Tapas in Barcelona when we finally arrived.

On Friday, we decided to visit a bit the city and again we had Tapas for lunch. During the evening, we joined the others at the welcome events with even more tapas, pizza and beers. It was really nice to see everyone again and meet some KDE
developers for the first time.

## The talks

Saturday I had to woke up early (around 9AM) for the first day of the actual conference. My personal highlight of the day was the goal presentation, as the KDE goal I proposed (KDE for all: Boosting accessibility) ended up being voted. Thanks a lot to the community for trusting me as a goal champion for the next tree years. I will post soon another blog post to talk in more details about the next steps.

In the evening, I ended up again eating some tapas and beers and then watched a [Correfoc](https://ca.wikipedia.org/wiki/Correfoc). It was impressive.

![Correfoc](correfoc.png)

On Sunday, we had 


## The BOFs