---
title: Accessibility Inspector 1.0
date: "2023-12-04T12:00:00Z"
draft: true
---

Accessibity Inspector 1.0 is out. This is the first release of this application as
a standalone component. Previously it was part 