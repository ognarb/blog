---
title: Announcing Brise theme
date: "2023-12-19T10:00:00Z"
hideHeaderImage: true
aliases:
 - /2023/12/20/announcing-brise-theme/
categories:
  - Free Software
  - KDE
tags:
  - KDE
image: separator.png
comments:
  host: floss.social
  username: kde
  id: 111607304711377695
---

<script
  defer
  src="/comparaison.js"></script>
<link
  rel="stylesheet"
  href="https://cdn.jsdelivr.net/npm/img-comparison-slider@8/dist/styles.css"
/>

<style>
:root {
  --divider-color: #fff;
}
@media (prefers-color-scheme: dark) {
:root {
  --divider-color: #fff;
}
}
</style>

[Brise theme](https://invent.kde.org/carlschwan/brise/) is yet another fork of
Breeze. The name comes having both the French and German translations of Breeze,
being Brise.

As some people know, I'm contributing quite a lot to the Breeze style for
the Plasma 6 release and I don't intend to stop doing that. Both git repositories
share the same git history and I didn't massively rename all the C++ classes from
BreezeStyle to BriseStyle to make it as easy as possible to backport commits
from one repository to the other. There are also no plans to make this the new
default style for Plasma.

My goal with this Qt style is to have a style that is not a big departure of
Breeze like you know it but does contain some cosmetic small changes. This
would serve as a place where I can experiment with new ideas and if
they tend to be popular to then move them to Breeze.

Here is a breakdown of all the changes I made so far.

- I made Brise coinstallable with Breeze, so that users can have both installed
  simultaneously. I minified the changes to avoid merge conflicts while
  doing so.

- I increased the border radius of all the elements from 3 pixels to 5 pixels.
This value is configurable between small (3 pixels), medium (5 pixels) and large
(7 pixels). A [merge request](https://invent.kde.org/plasma/breeze/-/merge_requests/388)
was opened in Breeze and might make it into Plasma 6.1. The only difference is that
in breeze the default will likely keep being 3 pixels for the time being.

![Cute buttons and frames with 5 pixels border radius](rounded.png)

- Add a separator between the search field and the title in the standard KDE
  config windows which serves as an extension of the separator between the list
  of the setting's categories and the setting's page. This is mostly to be similar to System
  Settings and other Kirigami applications. There is a pending merge
  request for this also in [Breeze](https://invent.kde.org/plasma/breeze/-/merge_requests/371).
- A new tab style that removes the blue lines from the active lines and
  introduce other small changes. Non-editable tabs are also now filling the
  entire horizontal space available. I'm not completely happy with the look
  yet, so no merge requests have been submitted to Breeze.

![Separator in the toolbar and the new tabs](separator.png)

- Remove outlines from menu and combobox items. My goal is to go in the same
  direction as KirigamiAddons.RoundedItemDelegate.

![Menu without outlines](menu.png)

- Ensure that all the controls have the same height. Currently a small disparency in height is noticeable when they are in the same row. The patch is still a bit hacky and needs some wider testing on a large range of apps to
  ensure no regressions, but it is also a improvement I will definitively submit upstream
  once I feel like it's ready.

![](alignment1.png)

![](alignment2.png)

Here, in these two screenshots, every control has 35 pixels as height.

Finally here is Kate and KMail's settings with Breeze and Brise.

<img-comparison-slider class="w-100" >
  <img slot="first" class="w-100" src="kate-breeze.png" />
  <img slot="second" class="w-100" src="kate-brise.png" />
  <svg slot="handle" xmlns="http://www.w3.org/2000/svg" width="100" viewBox="-8 -3 16 6">
    <path stroke="var(--divider-color)" d="M -5 -2 L -7 0 L -5 2 M -5 -2 L -5 2 M 5 -2 L 7 0 L 5 2 M 5 -2 L 5 2" stroke-width="1" fill="var(--divider-color)" vector-effect="non-scaling-stroke"></path>
  </svg>
</img-comparison-slider>


<img-comparison-slider class="w-100" >
  <img slot="first" class="w-100" src="kmail-breeze.png" />
  <img slot="second" class="w-100" src="kmail-brise.png" />
  <svg slot="handle" xmlns="http://www.w3.org/2000/svg" width="100" viewBox="-8 -3 16 6">
    <path stroke="var(--divider-color)" d="M -5 -2 L -7 0 L -5 2 M -5 -2 L -5 2 M 5 -2 L 7 0 L 5 2 M 5 -2 L 5 2" stroke-width="1" fill="var(--divider-color)" vector-effect="non-scaling-stroke"></path>
  </svg>
</img-comparison-slider>