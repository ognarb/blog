---
title: Matrix Community Summit and KDE Promo Sprint in Berlin
date: "2023-09-24T12:00:00Z"
hideHeaderImage: true
categories:
  - Free Software
  - KDE
tags:
  - KDE
  - Promo
image: cbase.jpg
comments:
  host: floss.social
  username: carlschwan
  id: 111122177994032421
---

On Thursday and Friday evenings, I went to the Matrix Community Summit at C-Base
in Berlin with Tobias. It was the occasion to meet a few other Matrix
developers particularly the Nheko developer, MTRNord and a few other devs
whom I only knew by nickname. It was great even though I could only spend a
few hours there. Tobias stayed longer and will be able to blog more about
the event.

![Photo of the C-Base showing a lot of electronical equipements](cbase.jpg)

During the weekend, instead of going to the Matrix summit, I participated to
the KDE Promo sprint with Paul, Aniqa, Niccolo, Volker, Joseph. Aron also
joined us via video call on Saturday. This event was also in Berlin at the KDAB
officem which we are very thankful for hosting us.

This sprint was the perfect occasion to move forward with many of our pending
tasks. I mainly worked on web-related projects as I tried to work on a few items
on my large todo list.

We now have an updated donation page, which includes the new donnorbox widget.
Donnorboy is now our preferred way to make recurring donations and recurring
donations are vital to the success of KDE. [Check it out!](https://kde.org/community/donations/)

![Screenshot of the website KDE.org/community/donations](donation.png)

With Paul, we also looked at the next [KDE For-pages](kde.org/for). Two of them
are now done and we will publish them in the coming weeks. There are plans for
a few more and if you want to get involved there, this is the
[phabricator task to follow](https://phabricator.kde.org/T13726).

I also updated the [KDE For Kids](https://kde.org/for/kids/) with the help of
Aniqa. It now features the book Ada & Zangemann from Matthias Kirschner
and Sandra Brandstätter that sensibilise kids to Free Software. Let me know if you have
other books suggestions for kids around Free Software and KDE that we can
include on our websites.

This was only a short version of all the things we did during this sprint, I will
let the others blog about what they did. More blog posts will certainly pop up
on [planet.kde.org](https://planet.kde.org) soon.

The sprint would have been only possible thanks to the generous donation from
our users, so [consider making a donation today](https://kde.org/community/donations/)!
Your donation also helps to pay for the cost of hosting conferences, server
infrastructure, and maintain KDE software.
