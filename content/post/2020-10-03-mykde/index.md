---
date: "2020-10-03T12:00:00Z"
locale: en
title: Announcing MyKDE
image: homepage.png
---

I'm happy to announce the successful deployment of the new identity system
in KDE, codename MyKDE. The new identity system is now available in
[my.kde.org](https://my.kde.org). You should be able to login into the my.kde.org website
with your normal KDE credential.

For the moment, only the wikis are using MyKDE but in the coming months
this should change with more and more services switching to MyKDE. I will
let you all know of the progress of the migration.

FAQ:
====

Why the move?
-------------

identity.kde.org is using OpenLDAP for user management with a small PHP
frontend allowing the account creation. And we had the following problems
with it:

* Account removal is hard, requiring significant manual intervention and
effort (several hours work in some instances)
* Account registration takes 30 seconds or more to complete, creating a poor
user experience
* Groups don't scale effectively
* Anti-spam measures are too crude

More on that in [T8449](https://phabricator.kde.org/T8449)

Will my data be migrated?
-------------------------

Yes, your data are migrated just by login into MyKDE once. This will migrate
all your group membership (KDE developer, Akademy Team, ...), personal
data and password.

For users who didn't log into MyKDE during the migration period. If you are
a KDE developer or KDE e.V. member, your account will be imported as a
disabled account and you will need to ask sysadmins to enabled it. For the
rest of the users of identity.kde.org who don't have a membership to groups,
your account will be removed. We think this is the best solution because there
is no need to store personal information that we don't need from users who
don't use the system anymore. If you want to conserve your account (and
username), please log at least once. We will send periodic emails reminding
you of migrating your account.

How do I register a new account in MyKDE?
-----------------------------------------

For the moment the possibility of registering a new account is disabled in
MyKDE and the only possibility is to create an identity.kde.org account and
then migrate your account. This is due to the fact we don't want some
accounts existing only in MyKDE. This will naturally change when we migrate
fully to MyKDE.

How to I collect a badge?
-------------------------

MyKDE has the possibility to grant badges to users. For the moment there is
only one badge enabled: KDE developer. This badge is given to every KDE
developer and is displayed on their public profile (if enabled). When the
new Season of KDE website will be deployed, it will be also possible to have
an SoK mentor and SoK mentee badge.

I'm interested in ideas of new badges (and badge designs), so please let me
know if you have a genius idea that doesn't gamify KDE contribution (e.g no made
100/1000/10 000 commits badge).

Note that you are in control of that badge get displayed.

What is the public profile functionality?
-----------------------------------------

One of the new features of MyKDE is the possibility to have a public profile.
This public profile is opt-in, so you need to explicitly enable it to make
it work and it can display a small bio, your avatar, name, username, social
network account, Liberapay account, and badges earned.

This is for example how it looks for [me](https://my.kde.org/user/carlschwan/)

Can I contribute to MyKDE?
--------------------------

Yes, the source code is hosted in [websites/my-kde-org](https://invent.kde.org/websites/my-kde-org)
and all the deployment information can be found in the
[KDE sysadmin documentation](https://sysadmin-docs.kde.org/services/mykde.html).
