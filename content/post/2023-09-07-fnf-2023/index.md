---
title: Freedom Not Fear 2023
date: "2023-09-07T16:00:00Z"
hideHeaderImage: true
categories:
  - Free Software
  - KDE
  - EU
tags:
  - KDE
  - EU
image: fnf-group.jpeg
comments:
  host: floss.social
  username: carlschwan
  id: 111007905414476676
---

Last weekend, I went to [Freedom Not Fear 2023](https://freedomnotfear.org/) in
Brussels. Fnf is an unconference for and by European digital activists. It
covers various topics, from the latest terrible European law (Chat Control) to
discussing how to get more involved in our democracies.

I usually attend more technical conferences, and it was refreshing to
participate in a conference where ethical and political discussions around
digital rights were a central topic. It was an occasion to meet people from
different backgrounds, from a Dutch politician (and self-proclaimed student
for life), to a member of various organizations (e.g. Edri, NlNet,
epicenter.works, Chatons, …) and journalists from Netzpolitik.

![Encryption is Love, Encryption is democracy, Encryption is Safety](love.jpg)

On Friday evening, aside from the welcome talk, we had a presentation from the
European Data Protection Supervisor (Wojciech Wiewiórowski) about their work on
deploying a Mastodon instance for the EU institutions and how the lack of
subscribers makes it hard to justify continuing investing in it.

The presentation is on [Peertube](https://tube.network.europa.eu/videos/watch/4bac7534-6ce4-4583-9e04-b32d94777c27)
if someone wants to watch it.

![Picture of Wojciech Wiewiórowski presenting his talk](mastodon.jpg)

During the weekend, we had an unconference-style conference where everyone could
create a topic of discussion and present their work interactively. This worked
very well.

![Board with all the discussion topics](topics.jpg)

Many participants were using Linux (and often with Plasma), but others were
unaware of KDE. So I did a small lighting talk about the KDE community and
presented a few utilities we create: Plasma, GCompris, Labplot, Krita, Merkuro
and Itinerary. Time was limited, so I couldn’t show everything we were doing,
but I hope this small list of software shows that we are covering many
different types of software.

I prepared my slides the day before, as I saw some slots for lightning talks
were still available, and the new [KDE For](https://kde.org/for) pages were of
great help. But it makes sense to have some slides provided by KDE Promo, which
can then be reused and modified depending on the audience. I’ll bring up the
idea at the next KDE Promo sprint in 2 weeks.

Aside from the weekend, which was packed with discussion, we went on Monday to
the European Parlament and had a small presentation about how the European
Parlament works. We also had the opportunity to ask Patrick Breyer from the
German Pirate Party questions.


![Picture of Carl Schwan in front of the European Parlament and a sign "Democraty in Action"](demo.jpg)

![Picture of the European Parlament](parlement.jpg)

I enjoyed this conference and thank the organizers and Digital Courage for
organizing this event and the two MEPs for using some of their travel allowance
to bring many people to Brussels.

![Group photo with most of the participants](fnf-group.jpeg)