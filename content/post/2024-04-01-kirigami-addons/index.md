---
title: Kirigami Addons 1.1.0
date: "2024-04-01T15:00:00Z"
hideHeaderImage: true
comments:
  host: floss.social
  username: carlschwan
  id: 112196640350393977
categories:
  - Free Software
  - KDE
tags:
  - KDE
image: formcarddialog.png
---

It's again time for a new Kirigami Addons release. Kirigami Addons is a
collection of helpful components for your QML and Kirigami applications.

## FormCard

I added a new FormCard delegate: `FormColorDelegate` which allow to select a
color and a new delegate container: `FormCardDialog` which is a new type of
dialog.

![FormCardDialog containing a FormColorDelegate in Marknote](formcarddialog.png)

Aside from these new components, Joshua fixed a newline bug in the `AboutKDE`
component and I updated the code examples in the API documentation.

## TableView

This new component is intended to provide a powerful table view on top of the
barebone one provided by QtQuick and similar to the one we have in our
QtWidgets application.

This was contributed by Evgeny Chesnokov. Thanks!

![TableView with resizable and sortable columns](tableview.png)

## Other components

The default size of `MessageDialog` was decreased and is now more appropriate.

![MessageDialog new default size](messagedialog.png)

James Graham fixed the autoplay of the video delegate for the maximized album
component.

## Packager section

You can find the package on
[download.kde.org](https://download.kde.org/stable/kirigami-addons/kirigami-addons-1.1.0.tar.xz.mirrorlist)
and it has been signed with my [GPG key](/gpg-02325448204e452a/).
