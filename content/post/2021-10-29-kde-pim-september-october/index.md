---
title: KDE PIM in September and October
date: "2021-10-29T10:00:35Z"
image: month-view-deco.png
---

In the months since the [KDE PIM July-August report](https://www.davidcbryant.net/wp/2021/08/29/july-august-in-kde-pim/)
there have been two patch releases for Kontact, and over 1400 changes made by
more than 40 contributors have been integrated. Here are some of the highlights.

## KOrganizer

A lot has happened over the last two months in KOrganizer development. First of
all, Friedrich has been working on reviving the calendar decoration feature.
This feature was introduced in 2007, but has since suffered from a lack of
maintenance and multiple regressions.

Decorations allow you to display additional information inside the week view.
Here is what it looked like back in 2007 when it was introduced.

![KOrganizer back in 2007](https://commit-digest.kde.org/issues/2007-06-17/files/korganizer_themed_decorations.png)

As part of this effort, the plugin that allowed KOrganizer to display the
current "Picture of the day" from the English Wikipedia, was ported to
Wikipedia's new API, and now works again. (KDE Gear 21.12)

![Picture of the day](picture-of-day.png)

Friedrich is also working on a new plugin that shows the moon phase inside the
week view. Pretty neat if you like astronomy. This feature should hopefully also
land in KDE Gear 21.12.

![Moon Phase](moon-phase.png)

The work on the Moon Phase plugin prompted Friedrich to add this feature to
KOrganizer's month view too. This feature is still under review but should
hopefully land in KDE Gear 21.12 as well.

![Month View Decoration](month-view-deco.png)

There are also ongoing discussions about making this plugin more generic, and,
in the future, adding support for these calendar decorations to [Kalendar](https://apps.kde.org/kalendar)
and the [Plasma Calendar applet](https://carlschwan.eu/2021/04/24/plasma-calendar-redesign/).

Glen Ditchfield has also been busy fixing many issues inside KOrganizer.

The event editor no longer creates duplicate tags ([Bug 441846](https://bugs.kde.org/441846)),
and the list used in the Event View and the _Find_ dialog do not automatically
re-sort themselves by starting date when the event editor alters an event
([441530](https://bugs.kde.org/441530)). The user's sorting preferences for
these lists are preserved, just like other preferences.

The _Find_ dialog also got better labels and tooltips. When an event displayed
in it gets modified, this gets immediately updated in the UI.

Glen also made several improvements to the _Month View_. In general, the layout
is now more compact ([Bug 435667](https://bugs.kde.org/435667)). Multiple-day
holidays are now correctly displayed as one multiple-day event instead of
several single-day events. A more visible change is that the current day is now
highlighted with your system theme's highlight color instead of grey ([420515](https://bugs.kde.org/420515)).

![Month view](korganizer-month-view.png)

Lastly, the sidebar item viewer displays the completion date and time of
completed to-dos, rather than just the date ([Bug 374774](https://bugs.kde.org/374774)).

Volker Krause removed the procedure and email alarms that allowed events to
execute commands and send emails due to the security risk they pose in
combination with shared calendars.

In term of documentation, David Bryant started cleaning up KOrganizer's docs,
like he previously did with KMail.

## KHolidays

Glen also updated the Canadian holidays inside KHolidays, and Ghalib Ahmed added
holiday information for Pakistan.

## KMail

Laurent Montel improved the emoji support in KMail. It is now easier to find
emojis thanks to a new emoji search feature, as well as a new recently-used
emojis tab.

![Emoji search](emoji.png)

Search is generally easier since multiple search fields now perform a
case-insensitive search.

In the email editor, warnings about too many recipients are now indicated by an
inline message (instead of a popup dialog), conforming to KDE's HIG.

![Warning](warning-email.png)

Additionally, a few bugs related to the formatting in the email editor were
fixed by Laurent Montel (Bug [419978](https://bugs.kde.org/419978),
[442416](https://bugs.kde.org/442416) and
[443108](https://bugs.kde.org/443108)).

Volker Krause made sure that KMail connects to an SMTP servers with TLS enabled
even when no login is required ([bug 423423](https://bugs.kde.org/423423)). He
also fixed an infinite SSL warning dialog loop that appeared when a connection
to an IMAP server was rejected with an invalid certificate ([bug 423424](https://bugs.kde.org/423424)).

Sandro Knauß also helped solve the above security vulnerabilities and started
working on supporting the Autocrypt-Draft-State header. This header is needed
when you want to store a draft and need to indicate whether it should be sent as
an encrypted mail or not.

Ingo added filtering by paths to folder selection. This is useful when you need
to copy or move a message to a folder, or when you need to jump to a folderas
you just type, say, "foo/bar" and you will narrow down the list of folders to
those containing the string "bar" that are subfolders of folders. This change
was funded by Intevation GmbH. ([Bug 443791](https://bugs.kde.org/show_debug.cgi?id=443791)).

![Filtering by paths](kmail-filter-by-path.png)

Finally, David Bryant is working on updating the credits sections in both the
KMail documentation and the _About_ dialog, since most of the people who started
contributing to KMail after 2010 aren't yet mentioned in the credits.

## Kleopatra

Kleopatra is KDE's certificate manager and makes it possible to manage X.509 and
OpenPGP certificats and smartcards. In the last two months, Ingo Klöcker worked
on improving the accessibility of the decryption and encryption of files. He
also created an AppImage for Kleopatra.

## KAlarm

David Jarvie fixed a few issues KAlarm was having when handling application
themes. The time edit spinbox now works correctly when using the Breeze theme
([bug 443062](https://bugs.kde.org/443062)) and, similarly, the sub-daily
recurrence editor should now be usable with the Fusion theme.

He also worked on using the translated forms of AM/PM when displaying
times This will make it more appropriate for certain locales.

## Kalendar

Kalendar is also making tons of progress. Clau aded a new week view and enabled
filtering by tags, Carl (me) worked on a QtQuick version of KCommandBar, Devin
Lin ported the mobile version to Kirigami.NavigationTabBar and Felipe Kinoshita
made the sidebar collapsible. This is just the tip of the iceberg, you can find
a more comprehensive list of all the changes in [Clau Cambra's blog](https://claudiocambra.com/).

<div class="embed-responsive embed-responsive-16by9">
  <video src="https://claudiocambra.com/wp-content/uploads/2021/10/simplescreenrecorder-2021-10-24_14.50.20.mp4" alt="Week view in Kalendar" loop autoplay muted></video>
</div>

Kalendar also passed the KDE Review process and you can expect a first release
soon!

## KDE Itinerary

KDE Itinerary, the ultimate companion app for all your trips, also received many
new features. Itinerary now provides navigation guides for walking and for
transfer sections of public transport elements, a new barcode scan mode, and
rental vehicle information.

![KDE itinerary transfer navigation instructions](https://www.volkerkrause.eu/assets/posts/98/kde-itinerary-transfer-navigation-instructions.png)

You can find more information about all of Itinerary's changes in Volker
Krause's latest [blog post](https://volkerkrause.eu/2021/10/01/kde-itinerary-august-september-2021.html).

## Deprecation work

A large focus remains on preparing for the upcoming migration to Qt6 and KF6.
This mainly consists of porting away from deprecated functionality in Qt and KDE
Frameworks, as well as moving to more modern build systems or C++ constructs.
While this does not have any visible impact yet, this will allow for a smoother
transition down the line. This work was largely done by Laurent Montel,
Alexander Lohnau, Friedrich W. H. Kossebau, Nicolas Fella and a few others.

## Help us make Kontact even better!

Check out some of our open junior jobs! They are simple, mostly
programming-focused tasks, but they don’t require any deep knowledge or
understanding of Kontact, so anyone can work on them. Feel free to pick any task
from the list, then get in touch with us! We’ll be happy to guide you and answer
all your questions. [Read more here...](https://www.dvratil.cz/2018/08/kde-pim-junior-jobs-are-opened/)

Thanks to Claudio Cambra and Paul Brown for the proofreading.
