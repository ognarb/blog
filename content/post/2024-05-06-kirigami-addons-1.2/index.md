---
title: Kirigami Addons 1.2
date: "2024-05-09T20:00:00Z"
hideHeaderImage: true
comments:
  host: floss.social
  username: carlschwan
  id: 112413145933562451
categories:
  - Free Software
  - KDE
tags:
  - KDE
image: floatingtoolbar.png
---

Kirigami Addons 1.2 is out with some accessibility fixes and one new component:
FloatingToolBar.

## Accessibility

During the accessibility sprint, there was an effort to ensure the date and
time pickers were actually accessible. Aside from improving the screen reader
support, this also allow to write Selenium integration tests which uses these
components in Itinerary. Thanks Volker, David Redundo and others for working
on this!

## FloatingToolBar

Mathis and I worked on a new addition to Kirigami Addons adding to the existing
`FloatingButton` and `DoubleFloatingButton` components. This component is perfect
to add tool to editing and drawing areas and can either contain a simple
`RowLayout`/`ColumnLayout` containing ToolButtons or a `Kirigami.ActionToolBar`.

```qml
import org.kde.kirigamiaddons.components
import org.kde.kirigami as Kirigami

FloatingToolBar {
    contentItem: Kirigami.ActionToolBar {
        actions: [
            Kirigami.Action {
                ...
            }
        ]
    }
}
```

![](floatingtoolbar.png)

## Dialogs

With the style used by `FormCardDialog` and `MessageDialog` merged in
[Kirigami](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1519) and soon in
[qqc2-desktop-style](https://invent.kde.org/frameworks/qqc2-desktop-style/-/merge_requests/405)
too, I did some changes to the `FormCardDialog` and `MessageDialog` to use the
same padding as `Kirigami.Dialog`.

MessageDialog now works better on mobile with the layout adapting itself to the dialog size.

![messagedialog with a mobile layout](messagedialog-mobile.png)

Aditionally similar to KMessageBox, MessageDialog has an optional "don't show again" option
which can be enabled by setting the `dontShowAgainName` property similar to the KMessageBox api.

I also prepared these two components to work as standalone windows which is likely to
come with [this Qt 6.8 change request](https://codereview.qt-project.org/c/qt/qtdeclarative/+/556010).

![Dialog in Qt 6.8](qt68.png)

## CategorizedSettings

Jonah fixed a bug where it would be impossible to escape the settings on mobile.

## Documentation

I added more screenshot to the API documentation and updated the TableView example app
to use a 'frameless' style.

![](tableview.png)

## Qt 6.7 support

This release also brings support for Qt 6.7 on Android as this release introduced
an API and ABI change to the Android code. Thanks Joshua for tackling this issue.