---
title: Health of the KDE community
date: "2021-04-29T18:05:35Z"
aliases:
 - /2021/04/29/kde-contributions-health/
image: graph-commits.png
hideHeaderImage: true
categories:
  - Free Software
  - KDE
  - Community
tags:
  - KDE
---

Today, I decided to look at KDE git history and look at the project's health as
a whole. It's inspired by the work of [Hans Petter Jansson for GNOME](https://hpjansson.org/blag/2020/12/16/on-the-graying-of-gnome/)
and use the tool he made ([fornalder](https://github.com/hpjansson/fornalder)).

**fornalder** is easy to use and the documentation in the readme was
beneficial. I don't know if this is because it was programmed in Rust but
**fornalder** was blazingly fast and most of the time spent during this
analysis was spent on cloning the repos.

These stats include all the extragear, plasma, frameworks and release service
repository as well as most of the KDE websites and a few KDE playground
projects I had on my hard drive. For example, it doesn't includes most of the
unmaintained projects (e.g. kdepimlibs, kdelibs, koffice, plasma-mediacenter,
...). Also important to note, is that this doesn't include translations at
all, since they are stored in SVN and added in the tarballs during the
releasing process.

## Active Contributors

![Number of contributors by years](graph.png)

The explaination for the colors is described by Hans Petter Janssson in his
blog post by:

> The stacked histogram above shows the number of contributors who touched the project on a yearly basis. Each contributor is assigned to a generational cohort based on the year of their first contribution. The cohorts tend to shrink over time as people leave.
> 
> There’s a special “drive-by” cohort (in a fetching shade of off-white) for contributors who were only briefly involved, meaning all their activity fits in a three-month window. It’s a big group. In a typical year, it numbers 200-400 persons who were not seen before or since. Most of them contribute a single commit.

This first plot is interesting. There is clear sharp increase in contributors
from 1997 to 2009, but from 2010 to 2019, the number of active contributors was
mostly stable with the exception of 2012 probably due to the fall of Nokia.

On the other side, 2020 attracted a great number of new contributors. I think
this is mostly due to the adoption of gitlab, making it way easier to submit
small patches, but the pandemic probably also played a role here.

## Commits Count

![Number of commits by years](graph-commits.png)

In this plot, the fall of Nokia is even more visible with a huge drop in
commits around that time. Fortunately, it seems in 2020 we were able to
recover and in terms of commits, we are at a new all-time high. The amount of
commits per year is particularly impressive. I first thought it was due
to a bug in the analyzer but just looking at the commits count in a few projects,
this makes perfect sense. There are 100 000 commits in Calligra, 55 000 commits
in Krita, 14 000 commits in Kdenlive, 22 000 commits in kde.org, 25 000 commits in KMail,
and a lot of other projects having other 5 000 commits probably explains the
numbers.

If you are wondering who in the 1999 cohort made most of the contributions,
it's Laurent Montel and who in the 2010 disappeared in 2018 it's "Montel Laurent".
Unfortunately I couldn't find a nice way to merge both. In term of commits,
Laurent Montel is the biggest contributor with nearly 60 000 commits and the
second one is "Montel Laurent" with nearly 45 000 commits.

## Conclusion

I would say that KDE is in relatively good health. This is good news
considering all the work ahead of us with the Qt6 and KF6 transition. I hope
these good trends continue in the future.

You can play with [the sqlite dabase](db.sqlite) (>200Mb)
