---
title: Kirigami Addons 0.5 release
date: "2022-10-23T11:40:35Z"
categories:
  - Free Software
  - KDE
tags:
  - KDE
---

Just a small note that Kirigami Addons 0.5 was released yesterday.
This update only contains some small cleanups here and there (e.g.
moving some implementation details from the public api to the private one).

The package is available on [download.kde.org](http://download.kde.org/stable/kirigami-addons/kirigami-addons-0.5.tar.xz.mirrorlist)
and was signed with my new [GPG key](/gpg-02325448204e452a/)