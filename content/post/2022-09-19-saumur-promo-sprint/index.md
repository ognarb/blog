---
title: Promo sprint in Saumur
date: "2022-09-19T10:00:00Z"
image: /2022/09/19/promo-sprint-in-saumur/sprint.jpg
hideHeaderImage: true
---

Last month, I was in Saumur (France) to attempt a KDE Promo sprint. This was my first sprint
since the pandemic, actually this might even be my first ever official KDE sprint as before
the pandemic I primarely attended conferences (Akademy, Fosdem, Libre Graphics Meeting, Linux
App Summit, ...) but no official sprint.

The sprint took place during the weekend and was a great occassion to meet Allon, Aron and
Neophytos for the first time. Aside from them I also meet Claudio, Paul, Joseph and Aniqua
would I had already had the chance to meet before.

While the sprint was only 2 days long, I think we had some really productive discussions
about the general strategie we should take and also how to move forward with some stuck
tasks.

Personally I was quite happy to unblock one of my previous idea of creating
"KDE for"-webpages. I already created a [KDE for Kids](https://kde.org/for/kids) page
a long time ago but never managed to find the time and motivation to create more of them.
So during the sprint, we started to brainstorm a bit for a "KDE for Creators" page, you
can already take a look at the wip prototype [here](api.carlschwan.eu/for/creators/) and
if you have suggestions and want to help we have a [phab task](https://phabricator.kde.org/T15770).

Aside from all the productive discussions, Allon made us discover Saumur. It's a really nice
city near the Loire. But I need to say that I was quite depressed at the level of water in the
Loire, it looked almost empty. Good reminder that climate change is real and human made.

Aside from the sad state of the Loire, we also tasted a lot of good food. We had some
gallete bretones on Sunday evening and it was delicious. Allon also invited us
Saturday night at his place and he made fouée for us.

![bread oven with fouée](fouee.jpg)

It's a local speciallity and it was really good and I was so full at the end of the day.
Thank you Allon and your family for being such wonderful hosts!

![Sprint photo](sprint.jpg)
