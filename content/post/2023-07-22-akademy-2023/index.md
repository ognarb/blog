---
title: Akademy 2023
date: "2023-07-24T18:00:00Z"
hideHeaderImage: true
categories:
  - Free Software
  - KDE
tags:
  - Community
  - KDE
image: park.jpg
comments:
  host: floss.social
  username: carlschwa
  id: 110770409059176757
---

Last week, I went to Akademy, the yearly [KDE](https://kde.org) conference, in
Thessaloniki. This is now my third in-person Akademy and fifth Akademy in
total. As always, this is the occasion to meet old and new friends, learn
about what others are hacking on and enjoy good food.

## Friday

I arrived Friday afternoon, taking my flight from Nuremberg with a few others.
Getting out of the airport, we could immediately feel the heat. After leaving
our stuff at the hotel, we went to join the rest of the KDE folks at the
welcome event in a frindly and cozy bar.

## Saturday

On Saturday, I talked about the current state of the Accessibility (a.k.a
KDE For All) goal. In short, we are making progress on that and fixing
bugs everywhere in the stack, but we desperately need more community
involvement as the task is big. Hopefully, the Selemiun AT-SPI bridge can be
helpful, as it makes it possible to write integration tests using the
accessibility API which require some accessibility support in your application
to work. ([Slides](https://conf.kde.org/event/5/contributions/146/attachments/89/115/accessibility.pdf))

![Me in a pannel with Joseph and Nate](talk.jpg)

I also presented the Fundraising WG report. We had two successful
fundraising campaigns this year: Kdenlive and the end of the year campaign.
([Slides](https://conf.kde.org/event/5/contributions/144/attachments/100/126/Fundraising%20WG%20Report.pdf))

My highlight for the day was the KDE Embedded talk from Andreas. The
talk explained the progress in getting the whole KDE software packaged as
Yocto layers, which already allows us to build images for RISC-V and ARM devices
with Plasma BigScreen. There is definitively a lot of potential there
to get KDE technologies in a wide varieties of devices. A recent example is
KWin being integrated inside Mercedes cars' infotainment system, resulting in
various upstream improvements.
([Slides](https://conf.kde.org/event/5/contributions/131/attachments/70/86/talk_embedded_final.pdf))

## Sunday

On Sunday, I had a talk with Tobias Fella about Matrix and Mastodon and how we
can integrate these protocols inside our applications in various ways. First,
by providing clients for these protocols: NeoChat (Matrix) and Tokodon (Mastodon),
but also with less oblivious ways like using Mastodon as a [comment system on your blog](https://carlschwan.eu/2020/12/29/adding-comments-to-your-static-blog-with-mastodon/)
or by using Matrix to share your KDE Itinerary live data end-to-end encrypted
with your friend without requiring a centralized server.
([Slides](https://conf.kde.org/event/5/contributions/134/attachments/91/107/Akademy2023SlideTemplate.pdf))

My highlight for Sunday was the Selenium GUI Testing talk from Harald,
which is closely related to my Accessibility goal and Automatisation goal from Nate.
([Slides](https://conf.kde.org/event/5/contributions/137/attachments/79/95/selenium.pdf))

The lightning talks were also excellent. Kai showed off his integration of
[AlphaCloud inside Plasma](https://github.com/kbroulik/qalphacloud) to get his
solar pannel stats. ([Slides](https://conf.kde.org/event/5/contributions/150/attachments/95/112/Fun%20with%20charts.pdf))

![KInfoCenter module showing both live and historic photovoltaic information](https://github.com/kbroulik/qalphacloud/raw/master/artwork/Screenshot_infocenter.png?raw=true)

Jean-Baptiste Kempf (from VLC) presented his cross-platform
remote control software stack in Rust, which promises very low latency.

Fabian Kosmale from the Qt <s>Company</s> Group presented the qmllint tool,
which should really helps improve the quality of our software and reduces the
risk to accidentally create regressions in our QML applications. I'm looking
forward to using it on my applications and to building custom plugins for it.
([Slides](https://conf.kde.org/event/5/contributions/151/attachments/82/98/qmllintwhat.pdf))

Finally, Nate presented the new Plasma Welcome wizard, which is great!
([Slides](https://conf.kde.org/event/5/contributions/148/attachments/88/104/Akademy%202023%20presentation%20Welcome%20Center.pdf))

## Bofs

The rest of the week, we had “Birds of a Feather” (or BoF) which were particularly
productive. I (co-)hosted multiple BoFs this year: Plasma Mobile, NeoChat, KDE PIM
and Accessibility, but I also enjoyed a few other BoFs: the fundraising BoF where Victoria gave us a
lot of advice on how to do fundraising for non-profit and the KDE e.V. board office
hour or the Selemiun BoFs from Harald and a few others.

![Myself hacking on my computer during a bof](bof.jpeg)

## Day trip

We went to Mount Olympus, Dion, and a beach for our day trip. As a ancient Greece nerd,
I enjoyed it! Here are some photos from the trip:

![Dion archelogical park](park.jpg)

![Dion Museum of archelogie](museum.jpg)

![Mount Olympus](olympus.jpg)

![Beach beer](beer.jpg)

## Trainings

We also had training at Akademy. I participated to the one from Nuno (thanks,
KDAB for offering it) about [Qt Design Studio](https://www.qt.io/product/ui-design-tools),
a very powerful QML visual editor and with a bridge to
Figma. Unfortunately, I couldn't attempt this training fully because it
conflicted with a BoF I was hosting, but the bit I saw there was very interesting.
It would be great if we could integrate Kirigami components inside this tool.

![Nuno presenting his training](nuno.jpg)
