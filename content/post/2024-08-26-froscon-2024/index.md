---
title: FrOScon 2024
date: "2024-08-26T8:45:00Z"
categories:
  - Free Software
image: gnupg.jpg
hideHeaderImage: true
---

This year, I attended [FrOScon](https://froscon.org/en/) for the first time .
FrOScon is the biggest conference about free and open-source software in
Germany. It takes place every year in Bonn/Siegburg (Germany) at the
weekend and is free to attend.

For the first time, I was not at a conference to staff a KDE stand. My employer
had a stand there, and it was a great occasion for me to meet some colleagues,
fellow KDE, and Matrix contributors.

![GnuPG Stand](gnupg.jpg)

So I spent the majority of my time at the GnuPG stand and discussing many things
with Volker, including KDE PIM and the future of KWallet.

I also meet many Matrix community members and am excited to attend the
[Matrix Conference](https://2024.matrix.org/) next month.

![Matrix Stand](matrix.jpg)

All in one, it was a great conference and I hope to see more KDE people there
next year and maybe even having out own KDE stand.