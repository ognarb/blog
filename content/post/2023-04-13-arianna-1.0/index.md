---
title: Announcing Arianna 1.0
date: "2023-04-13T12:00:00Z"
hideHeaderImage: true
categories:
  - Free Software
  - KDE
  - Arianna
tags:
  - Arianna
  - KDE
image: library-view.png
comments:
  host: floss.social
  username: kde
  id: 110190525463742605
---

I'm happy to announce the initial release of Arianna. Arianna is a small ePub
reader application I started with Niccolo a few months ago. Like most of my
open source applications, it is built on top of Qt and Kirigami.

Arianna is both an ePub viewer and a library management app. Internally,
Arianna uses Baloo to find your existing ePub files in your device and
categorize them.

![Library view](library-view.png)

The library view will keep track of your reading progress and find new books as
soon as you download them.

If your book library is particularly big, you can either use the internal
search functionality, or look through the various categories and find books
grouped by genre, publisher or author.

![Library search popup showing a few search results](library-search.png)

![Library grouped by authors](library-author.png)

The actual reader is quite basic as its only function is to show the content of
the book. That said, it does have features like a progress bar to keep track of
your reading progress and also lets you navigate within the book.

It is also fully navigable with the keyboard.

![Ebook reader showing the book content](reader.png)

Another feature allows you to search within a book for a specific word.

![Ebook reader showing the book content](reader-search.png)

## Get it

Arianna will soon be available in Flathub (once the submittion is accepted).
Please also ask your distribution to package Arianna.

## Credits

This application would have not been possible without the previous work carried
out by Foliate, from whom I copied and adapted the epub.js integration, and
Peruse from whom I copied and adapted the library management code. Finally I
would like to thank Šimon Rataj  who made numerous contribution and fixed
multiple bugs.

Arianna is also translated in multiple languages, thanks to some wonderful
translators. Here is the alphabethically sorted list: Basque, British English,
Catalan, Czech, Dutsch, Finish, French, German, Georgian, Hungarian,
Interlingua, Mandarin, Portuguese, Slovak, Spanish, Turkish, Ukrainian and
Valencian.

## Get Involved

If you are interested in helping, don't hesitate to reach out in the Arianna
matrix channel ([#arianna:kde.org](https://matrix.to/#/#arianna:kde.org)) and I
will be happy to guide you.

I also regularly post about my progress on Arianna (and other KDE apps) on my
[Mastodon account](https://floss.social/@carlschwan), so don't hesitate to
follow me there ;)

And in case, you missed it, as a member of KDE's fundraising working group, I
need to remind you that KDE e.V., the non-profit behind the KDE community
accepts [donations](https://kde.org/fundraisers/yearend2022/).

## Packager section

You can find the package on
[download.kde.org](https://download.kde.org/stable/arianna/arianna-1.0.0.tar.xz.mirrorlist)
and it has been signed with my [GPG key](/gpg-02325448204e452a/).