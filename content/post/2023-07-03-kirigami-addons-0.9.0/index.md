---
title: Kirigami Addons 0.9.0
date: "2023-07-03T06:00:00Z"
hideHeaderImage: true
categories:
  - Free Software
  - KDE
  - KirigamiAddons
tags:
  - KirigamiAddons
  - KDE
image: form-grid-container.png
comments:
  host: floss.social
  username: carlschwan
  id: 110648764588042496
---

It's time for a new Kirigami Addons release. This new release contains the work
of Joshua Goins, Laurent Montel, Thiago Sueto, Volker Krause, Shubham Arora,
James Graham, Rishi Kumar and myself. Thanks for contributing!

## New features

### MobileForm.FormGridContainer

FormGridContainer makes it possible to make the information exposed with
MobileForm more compact by putting multiple small cards in the same row. The
number of columns by default is 3 on desktop unless the total number of cards
makes it more sensible only to use 2 columns (e.g when there are only 2 or 4
elements).

On mobile the number of columns is reduced to only 2.

This new component was initially made by Rishi Kumar for the info grid that Tokodon, with some further improvement from myself. Here is how this looks with
Tokodon.

![Info Grid](form-grid-container.png)

And the current API:

```qml
import org.kde.kirigamiaddons.labs.mobileform 0.1 as MobileForm

MobileForm.FormGridContainer {
    infoCards: [
        MobileForm.FormGridContainer.InfoCard {
            title: "42"
            subtitle: i18nc("@info:Number of Posts", "Posts")
        },
        MobileForm.FormGridContainer.InfoCard {
            title: "42"
        },
        MobileForm.FormGridContainer.InfoCard {
            // Clickable card
            title: "Details"
            action: Kirigami.Action {
                onClicked: pageStack.push("Details.qml")
            }
        }
    ]
}
```

### MobileForm.AboutPage now can contains extra content

Applications can now extend the `AboutPage` to add extra content to it. This
was driven by the need of Itinerary which need to expose the license
information about the open data it uses.

![About page of itinerary](about.png)

### Validor support in MobileForm.FormTextFieldDelegate

Application can now add a Validor to their textfield. This was driven by the
need of Keysmith rewrite to use MobileForm.

![About new 2fa with Keysmith](keysmith.png)

## Important bugfixes

The BasicTreeItem component now uses correct spacing between items. This was caused by a regression when adding right to left support to the KirigamiAddons TreeView.

The native Android date/time pickers now works correctly even if multiple instance of it are loaded at the same time.

Shubham and James fixed various bugs with the maximized image and video component which is displayed in NeoChat and Tokodon when viewing media files.